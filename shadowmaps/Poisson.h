#pragma once

/*
Poisson disk samples.
*/

#ifndef SHADOW_POISSON_H
#define SHADOW_POISSON_H

#include "cyPoint.h"

extern float poissonDisk16[32];
extern float poissonDisk56[112];
extern float poissonDisk100[200];

// --------------------------------------------------------
// Random Functions
// --------------------------------------------------------

float random(cyPoint3f a, int b);
cyPoint2f RandSample16(int i, const cyPoint3f &p);
cyPoint2f RandSample100(int i, const cyPoint3f &p);

#endif