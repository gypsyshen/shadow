#version 330 core

// ---------------------------------------------------------
// Interpolated values from the vertex shaders
// ---------------------------------------------------------
in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;
in vec4 ShadowCoord;

// ---------------------------------------------------------
// Ouput data
// ---------------------------------------------------------
layout(location = 0) out vec3 color;

// ---------------------------------------------------------
// Uniform values that stay constant for the whole mesh.
// ---------------------------------------------------------
uniform sampler2D myTextureSampler;
uniform vec3 LightPosition_worldspace;

// number of shadowmaps
// int numShadowmap = 10;
uniform int numShadowmap;
// high -> low. i.e. 1024 -> 1
uniform sampler2D shadowMap;
uniform float shadowmapSize[11];
uniform float shadowmapFilterSize[11];
// level colors
//uniform vec3 levelColor[11];

// poisson disk samples
uniform vec2 poissonDisk16[16];
uniform vec2 poissonDisk100[100];

// number of samples
uniform int numSamples;

// which layer to display
uniform int layer;

// light size
uniform float lightSize;

// display mode: 0 - regular, 1 - low level, 2 - high level
//uniform int displayMode;
// display mode: 0 - shadow, 1 - weight
uniform int displayMode;

// diffuse color
uniform vec3 diffuse;

// filter type. 0: random sampling. 1: bicubic. 2: bilinear
uniform int filterType;

// light fov
uniform float lightFOV;

// depth bias params
#define BIAS_CONST_FACTOR 0.0005f
#define PI 3.1415926f

/*
// adaptive depth bias
vec3 n = normalize( Normal_cameraspace );
vec3 l = normalize( LightDirection_cameraspace );
float cosTheta = clamp( dot( n, l ), 0.f, 1.f );
float ratio = BIAS_CONST_FACTOR * shadowmapSize[layer] / shadowmapSize[0];
float bias = ratio * (BIAS_CONST_FACTOR + tan( acos(cosTheta)));
*/

// --------------------------------------------------------
// Random Functions
// --------------------------------------------------------

float random(vec3 a, int b) {
	vec4 seed4 = vec4(a, b);
	float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
    return fract(sin(dot_product) * 43758.5453);
}

vec2 RandSample16(int i) {

	int index = int(16.0 * random(gl_FragCoord.xyy, i)) % 16;
	return poissonDisk16[index];
}

vec2 RandSample100(int i) {
	
	int index = int(100.0 * random(gl_FragCoord.xyy, i)) % 100;
	return poissonDisk100[index];
}

// --------------------------------------------------------
// Depth Lookup, Depth Bias, Depth Test
// --------------------------------------------------------

float LookUpDepth(int level, vec2 uv) {
	// returns the texel value in sampler at uv.
	return textureLod( shadowMap, uv, level ).z;
}

float DistanceToLight() {
	// Returns the distance from current fragment to the light.
	return ShadowCoord.z / ShadowCoord.w;
}

vec2 DepthUV() {
	//	Returns the uv coordinate of current fragment in light space.		
	return ShadowCoord.xy/ShadowCoord.w;
}

vec2 DepthGradient(vec2 uv, float z) {
	// Derivatives of light-space depth with respect to texture coordinates.

    vec2 dz_duv = vec2(0.f, 0.f);

    vec3 duvdist_dx = dFdx(vec3(uv, z));
    vec3 duvdist_dy = dFdy(vec3(uv, z));

    dz_duv.x = duvdist_dy.y * duvdist_dx.z;
    dz_duv.x -= duvdist_dx.y * duvdist_dy.z;
    
    dz_duv.y = duvdist_dx.x * duvdist_dy.z;
    dz_duv.y -= duvdist_dy.x * duvdist_dx.z;

    float det = (duvdist_dx.x * duvdist_dy.y) - (duvdist_dx.y * duvdist_dy.x);
    dz_duv /= det;

    return dz_duv;
}

float BiasedZ(float z, vec2 dz_duv, vec2 offset, float ratio) {

	return z + ratio * dot(dz_duv, offset);
}

float Visible(float depth, float receiver) {
	/*
	Compute the visibility of a receiver.

	@params depth: reference depth.
	@params receiver: distance of receiver.
	@params layer: mipmap layer

	@return : visibility. 0: occluded; 1: visible.
	*/

	if( depth < receiver ) return 0.f; // receiver is blocked, it's in shadow
	else return 1.f;
}

// --------------------------------------------------------
// Find filter region in world space
// --------------------------------------------------------

vec4 FilterRegion(vec2 uv, int n_samples) {

	vec4 filter_region;
	float u0_sc, v0_sc, u1_sc, v1_sc;
	{
		vec2 uv_center;
		if(n_samples % 2 == 0) {
			uv_center = vec2(round(uv.x), round(uv.y));
			filter_region.x = uv_center.x - 0.5f - (0.5f * float(n_samples) - 1.f);
			filter_region.y = uv_center.y - 0.5f - (0.5f * float(n_samples) - 1.f);
		}
		else {
			uv_center = vec2(round(uv.x), round(uv.y));
			if(uv_center.x < uv.x) uv_center.x += 0.5f;
			else uv_center.x -= 0.5f;
			if(uv_center.y < uv.y) uv_center.y += 0.5f;
			else uv_center.y -= 0.5f;
			filter_region.x = uv_center.x - floor(0.5f * float(n_samples));
			filter_region.y = uv_center.y - floor(0.5f * float(n_samples));
		}

		filter_region.z = filter_region.x + float(n_samples-1);
		filter_region.w = filter_region.y + float(n_samples-1);
	}

	return filter_region;
}

// --------------------------------------------------------
// Estimate Filter Size
// --------------------------------------------------------

float EstimateFilterSize_PCSS(float receiver, float blocker, float lightSize) {

	float penumbraSize = (receiver - blocker) * lightSize / blocker;
//	if(penumbraSize < 0.f) penumbraSize = 0.f;

	// filter size (proportional to penumbra width)
	float filterSize = penumbraSize;

	return filterSize;
}

float EstimateFilterSize_Cem(float receiver, float blocker, float lightSize) {

	// blocker area in the light cone to explore
	float r_B = lightSize * (receiver - blocker) / receiver;
	// geometry size at the blocker distance as seen from the light center
	float r_G = 2.f * blocker * tan(lightFOV * 0.5f);
	// the corresponding filter size of the blocker distance
	float filterSize = r_B / r_G;

	return filterSize;
}

// --------------------------------------------------------
// Estimate Layer Weight
// --------------------------------------------------------

float EstimateLayerWeight(int layer, float blocker, float receiver, int n_samples) {

	float r = shadowmapSize[layer] * EstimateFilterSize_Cem(receiver, blocker, lightSize);
	float r_i = float(n_samples);

	// weight
	float weight = 0.f;	

	if(r < r_i) {
		if(layer == 0) {
			if(receiver - blocker < 0.001) weight = 0.f;
			else weight = 1.f;
		}
		else if(r > r_i / 2.f) {
			weight = 1.f - (r_i - r) / (0.5f * r_i);
		}
	}
	else {
		if(layer == numShadowmap - 1) weight = 1.f;
		else if(r < r_i * 2.f) {
			weight = 1.f - (r - r_i) / r_i;
		}
	}

	weight = clamp(weight, 0.f, 1.f);

	return weight;
}


// --------------------------------------------------------
// Triangular Filter
// --------------------------------------------------------

float Triangular(vec2 x0, vec2 x, float width) {

	float dist = distance(x, x0);
	
	// ignore values outside the filter region
	if(dist > width) return 0.f;

	return (width - dist) / width;
}

void PCF_Weight_TriangularFilter(out float pcf, out float weight, 
			int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {
			
	// scaled uv coordinate
	vec2 uv_sc = uv * smSize;	

	// starting point of the filter region
	vec4 filter_region_sc = FilterRegion(uv_sc, numSamples);

	//

	if(filter_region_sc.x >= 0.5f && filter_region_sc.y >= 0.5f 
		&& filter_region_sc.z <= smSize - 0.5f && filter_region_sc.w <= smSize - 0.5f) {
		
		// inverse of shadowmap size
		float invSMSize = 1.f / smSize;
		
		// sigma: width of the filter region
		float sigma = 0.5f * float(numSamples);

		// triangular filtering
		pcf = 0.f;
		weight = 0.f;
		float sum_f = 0.f;
		for(int j = 0; j < numSamples; ++j) {
			for(int i = 0; i < numSamples; ++i) {
				vec2 uv_sample_sc = vec2(filter_region_sc.x + float(i), filter_region_sc.y + float(j));
				vec2 uv_sample = uv_sample_sc * invSMSize;
				float depth = LookUpDepth( layer, uv_sample );
				float s = Visible(depth, receiver);
				float f = Triangular(uv_sc, uv_sample_sc, sigma);
				float w = EstimateLayerWeight(layer, depth, receiver, numSamples);
				pcf += s * f;
				weight += w * f;
				sum_f += f;
			}
		}
		pcf /= sum_f;
		weight /= sum_f;
	}
	else {
		pcf = 1.f;
		weight = 0.f;
	}
}

// --------------------------------------------------------
// Gaussian Filter
// --------------------------------------------------------

float Gaussian_Rec(vec2 x0, vec2 x, float sigma) {
	
	float dist_sq = (x.x-x0.x)*(x.x-x0.x) + (x.y-x0.y)*(x.y-x0.y);
	
	float sigma_sq = sigma * sigma;
	return 1.f/(2.f*PI*sigma_sq) * exp(-dist_sq/(2.f*sigma_sq) );
}

void PCF_Weight_GaussianFilter(out float pcf, out float weight, 
			int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {
			
	// scaled uv coordinate
	vec2 uv_sc = uv * smSize;	

	// starting point of the filter region
	vec4 filter_region_sc = FilterRegion(uv_sc, numSamples);

	if(filter_region_sc.x >= 0.5f && filter_region_sc.y >= 0.5f 
		&& filter_region_sc.z <= smSize - 0.5f && filter_region_sc.w <= smSize - 0.5f) {
		
		// inverse of shadowmap size
		float invSMSize = 1.f / smSize;
		
		// sigma: width of the filter region
		float sigma = 0.125f * float(numSamples);

		// gaussian filtering
		pcf = 0.f;
		weight = 0.f;
		float sum_f = 0.f;
		for(int j = 0; j < numSamples; ++j) {
			for(int i = 0; i < numSamples; ++i) {
				vec2 uv_sample_sc = vec2(filter_region_sc.x + float(i), filter_region_sc.y + float(j));
				vec2 uv_sample = uv_sample_sc * invSMSize;
				float depth = LookUpDepth( layer, uv_sample );
				float s = Visible(depth, receiver);
				float f = Gaussian_Rec(uv_sc, uv_sample_sc, sigma);
				float w = EstimateLayerWeight(layer, depth, receiver, numSamples);
				pcf += s * f;
				weight += w * f;
				sum_f += f;
			}
		}
		pcf /= sum_f;
		weight /= sum_f;
	}
	else {
		pcf = 1.f;
		weight = 1.f;
	}
}

// --------------------------------------------------------
// Bilinear Filter
// --------------------------------------------------------

float Bilinear(float coord_u0, float coord_u1, float coord_v0, float coord_v1, vec2 coord_uv, 
					float value_00, float value_01, float value_10, float value_11) {
	// Bilinear interpolation.

	float weight_u0 = (coord_u1 - coord_uv.x) / (coord_u1 - coord_u0);
	float weight_u1 = 1.f - weight_u0;
	float weight_v0 = (coord_v1 - coord_uv.y) / (coord_v1 - coord_v0);
	float weight_v1 = 1.f - weight_v0;

	float result = value_00 * weight_u0 * weight_v0
				 + value_10 * weight_u1 * weight_v0
				 + value_01 * weight_u0 * weight_v1
				 + value_11 * weight_u1 * weight_v1;

	return result;
}

void PCF_Weight_BilinearFilter(out float pcf, out float weight,
			int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

	// @params uv_receiver : texture coordinate of the receiver

	float invSMSize = 1.f / smSize;
	
	vec2 uv_sc = uv * smSize;
	
	float u0_sc = round(uv_sc.x) - 0.5f;
	float v0_sc = round(uv_sc.y) - 0.5f;
	float u1_sc = round(uv_sc.x) + 0.5f;
	float v1_sc = round(uv_sc.y) + 0.5f;

	// ignore pixels located in shadowmap boundaries
	if(u0_sc >= 0.5f && v0_sc >= 0.5f && u1_sc <= smSize - 0.5f && v1_sc <= smSize - 0.5f) {
			
		float u0 = u0_sc * invSMSize;
		float u1 = u1_sc * invSMSize;
		float v0 = v0_sc * invSMSize;
		float v1 = v1_sc * invSMSize;

		float depth00 = LookUpDepth( layer, vec2(u0, v0) );
		float depth01 = LookUpDepth( layer, vec2(u0, v1) );
		float depth10 = LookUpDepth( layer, vec2(u1, v0) );
		float depth11 = LookUpDepth( layer, vec2(u1, v1) );		
		
//		float biasedReceiver00 = BiasedZ(receiver, dz_duv, vec2(u0 - uv_receiver.x, v0 - uv_receiver.y), layerRatio);
//		float biasedReceiver01 = BiasedZ(receiver, dz_duv, vec2(u0 - uv_receiver.x, v1 - uv_receiver.y), layerRatio);
//		float biasedReceiver10 = BiasedZ(receiver, dz_duv, vec2(u1 - uv_receiver.x, v0 - uv_receiver.y), layerRatio);
//		float biasedReceiver11 = BiasedZ(receiver, dz_duv, vec2(u1 - uv_receiver.x, v1 - uv_receiver.y), layerRatio);
		float V00 = Visible(depth00, receiver);
		float V01 = Visible(depth01, receiver);
		float V10 = Visible(depth10, receiver);
		float V11 = Visible(depth11, receiver);

		float w00 = EstimateLayerWeight(layer, depth00, receiver, 2);
		float w01 = EstimateLayerWeight(layer, depth01, receiver, 2);
		float w10 = EstimateLayerWeight(layer, depth10, receiver, 2);
		float w11 = EstimateLayerWeight(layer, depth11, receiver, 2);

		pcf = Bilinear(u0_sc, u1_sc, v0_sc, v1_sc, uv_sc, V00, V01, V10, V11);
		weight = Bilinear(u0_sc, u1_sc, v0_sc, v1_sc, uv_sc, w00, w01, w10, w11);
	}
	else {
		pcf = 1.f;
		weight = 1.f;
	}
}

float PCF_BilinearFilter(int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

	// @params uv_receiver : texture coordinate of the receiver

	float invSMSize = 1.f / smSize;
	
	vec2 uv_sc = uv * smSize;
	
	float u0_sc = round(uv_sc.x) - 0.5f;
	float v0_sc = round(uv_sc.y) - 0.5f;
	float u1_sc = round(uv_sc.x) + 0.5f;
	float v1_sc = round(uv_sc.y) + 0.5f;

	// ignore pixels located in shadowmap boundaries
	if(u0_sc >= 0.5f && v0_sc >= 0.5f && u1_sc <= smSize - 0.5f && v1_sc <= smSize - 0.5f) {

		float u0 = u0_sc * invSMSize;
		float u1 = u1_sc * invSMSize;
		float v0 = v0_sc * invSMSize;
		float v1 = v1_sc * invSMSize;

		float depth00 = LookUpDepth( layer, vec2(u0, v0) );
		float depth01 = LookUpDepth( layer, vec2(u0, v1) );
		float depth10 = LookUpDepth( layer, vec2(u1, v0) );
		float depth11 = LookUpDepth( layer, vec2(u1, v1) );		
		
//		float biasedReceiver00 = BiasedZ(receiver, dz_duv, vec2(u0 - uv_receiver.x, v0 - uv_receiver.y), layerRatio);
//		float biasedReceiver01 = BiasedZ(receiver, dz_duv, vec2(u0 - uv_receiver.x, v1 - uv_receiver.y), layerRatio);
//		float biasedReceiver10 = BiasedZ(receiver, dz_duv, vec2(u1 - uv_receiver.x, v0 - uv_receiver.y), layerRatio);
//		float biasedReceiver11 = BiasedZ(receiver, dz_duv, vec2(u1 - uv_receiver.x, v1 - uv_receiver.y), layerRatio);
		float V00 = Visible(depth00, receiver);
		float V01 = Visible(depth01, receiver);
		float V10 = Visible(depth10, receiver);
		float V11 = Visible(depth11, receiver);
		
		float pcf = Bilinear(u0_sc, u1_sc, v0_sc, v1_sc, uv_sc, V00, V01, V10, V11);

		return pcf;
	}
	else return 1.f;
}

// --------------------------------------------------------
// Bicubic Filter
// --------------------------------------------------------

float Cubic(float x0, float x1, float dx0, float dx1, float x) {
    float a = 2.f * x0 - 2.f * x1 + dx0 + dx1;
    float b = -3.f * x0 + 3.f * x1 - 2.f * dx0 - dx1;
    float c = dx0;
    float d = x0;
    return a * pow(x, 3) + b * pow(x, 2) + c * x + d;
}

float BiCubic(float values[16], vec2 uv, vec4 filter_region) {

	// du, dv, and duv
	float dx[4];
	float dy[4];
	float dxy[4];
	
	for(int y = 0; y < 2; ++y) {
		for(int x = 0; x < 2; ++x) {
			int index = y * 2 + x; // index for dx, dy and dxy
			int xv = x + 1, yv = y + 1; // index for visibility
			dx[index] = (values[yv*4+xv+1] - values[yv*4+xv-1]) * 0.5f;
			dy[index] = (values[(yv+1)*4+xv] - values[(yv-1)*4+xv]) * 0.5f;
			dxy[index] = (values[(yv+1)*4+xv+1] - values[(yv-1)*4+xv] - values[yv*4+xv-1] + values[yv*4+xv]) * 0.25f;
		}
	}

	// begin filtering
	float result = 0.f;
	{
		float x = uv.x - filter_region.x - 1.f;
		float y = uv.y - filter_region.y - 1.f;

		float c1, c2;
    
		// f(x, y0) using f(x0, y0), f(x1, y0), dxf(x0, y0), dxf(x1, y0)
		c1 = values[(0+1) * 4 + (0+1)]; // img.pixels[y0*W+x0];
		c2 = values[(0+1) * 4 + (1+1)]; // img.pixels[y0*W+x1];
		float r_xy0 = Cubic(c1, c2, dx[0*2+0], dx[0*2+1], x);
    
		// f(x, y1) using f(x0, y1), f(x1, y1), dxf(x0, y1), dxf(x1, y1)
		c1 = values[(1+1)*4+(0+1)]; // img.pixels[y1*W+x0];
		c2 = values[(1+1)*4+(1+1)]; // img.pixels[y1*W+x1];
		float r_xy1 = Cubic(c1, c2, dx[1*2+0], dx[1*2+1], x);
    
		// dyf(x, y0) using dyf(x0, y0), dyf(x1, y0), dxyf(x0, y0), dxyf(x1, y0)
		float r_dy_xy0 = Cubic(dy[0*2+0], dy[0*2+1], dxy[0*2+0], dxy[0*2+1], x);
    
		// dyf(x, y1) using dyf(x0, y1), dyf(x1, y1), dxyf(x0, y1), dxyf(x1, y1)
		float r_dy_xy1 = Cubic(dy[1*2+0], dy[1*2+1], dxy[1*2+0], dxy[1*2+1], x);
    
		// f(x, y) using f(x, y0), f(x, y1), dyf(x, y0), dyf(x, y1)
		result = Cubic(r_xy0, r_xy1, r_dy_xy0, r_dy_xy1, y);
		result = clamp(result, 0.f, 1.f);
	}

	return result;
}

void PCF_Weight_BicubicFilter(out float pcf, out float w,
	int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

	float invSMSize = 1.f / smSize;

	vec2 uv_sc = uv * smSize;
	
	vec4 filter_region_sc = FilterRegion(uv_sc, 4);

	if(filter_region_sc.x >= 0.5f && filter_region_sc.y >= 0.5f
		&& filter_region_sc.z <= smSize - 0.5f && filter_region_sc.w <= smSize - 0.5f) {
		
		// 1. record visibilities of the 16 texels
		float visibility[16];
		float weights[16];
		{
			int k = 0;
			for(int y = 0; y < 4; ++y) {
				for(int x = 0; x < 4; ++x) {
					vec2 uv_neighbor = vec2(filter_region_sc.x + x, filter_region_sc.y + y) * invSMSize;
					float depth = LookUpDepth( layer, uv_neighbor );
					visibility[k] = Visible(depth, receiver);
					weights[k] = EstimateLayerWeight(layer, depth, receiver, 4);
					k++;
				}
			}
		}
		
		pcf = BiCubic(visibility, uv_sc, filter_region_sc);
		w = BiCubic(weights, uv_sc, filter_region_sc);
	}
	else {
		pcf = 1.f;
		w = 1.f;
	}
}

float PCF_BicubicFilter(int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

	float invSMSize = 1.f / smSize;

	vec2 uv_sc = uv * smSize;
	
	vec4 filter_region_sc = FilterRegion(uv_sc, 4);

	if(filter_region_sc.x >= 0.5f && filter_region_sc.y >= 0.5f
		&& filter_region_sc.z <= smSize - 0.5f && filter_region_sc.w <= smSize - 0.5f) {
		
		// 1. record visibilities of the 16 texels
		float visibility[16];
		{
			int k = 0;
			for(int y = 0; y < 4; ++y) {
				for(int x = 0; x < 4; ++x) {
					vec2 uv_neighbor = vec2(filter_region_sc.x + x, filter_region_sc.y + y) * invSMSize;
					float depth = LookUpDepth( layer, uv_neighbor );
					visibility[k] = Visible(depth, receiver);
					k++;
				}
			}
		}
		
		float pcf = BiCubic(visibility, uv_sc, filter_region_sc);

		return pcf;
	}
	else {
		return 1.f;
	}
}


// --------------------------------------------------------
// Pyramidal Filter
// --------------------------------------------------------

void PCF_Weight_Pyramidal(out float pcf, out float weight,
	int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

/*	float invSMSize = 1.f / smSize;
	vec2 uv_sc = uv * smSize;
	
	// x, y: coordinate
	// u, v: texture center

	float x0_mask_sc = uv_sc.x - 0.5f * float(numSamples);
	float x1_mask_sc = uv_sc.x + 0.5f * float(numSamples);
	float y0_mask_sc = uv_sc.y - 0.5f * float(numSamples);
	float y1_mask_sc = uv_sc.y + 0.5f * float(numSamples);

	if(x0_mask_sc >= 0.5f && y0_mask_sc >= 0.5f && x1_mask_sc <= smSize - 0.5f && y1_mask_sc <= smSize - 0.5f) {

		float height = 1.f / float(numSamples * numSamples);

		float x0_sc = floor(x0_mask_sc);
		float y0_sc = floor(y0_mask_sc);
		float x1_sc = floor(x1_mask_sc) + 1.f;
		float y1_sc = floor(y1_mask_sc) + 1.f;

		float u0_sc = x0_sc + 0.5f;
		float v0_sc = y0_sc + 0.5f;

		pcf = 0.f;
		weight = 0.f;
		for(int y = 0; y <= numSamples; ++y) {
			for(int x = 0; x <= numSamples; ++x) {
				float u_sample_sc = u0_sc + x;
				float v_sample_sc = v0_sc + y;

				vec2 uv_sample = vec2(u_sample_sc, v_sample_sc) * invSMSize;
				float depth = LookUpDepth( layer, uv_sample );
				float s = Visible(depth, receiver);
				float w = EstimateLayerWeight(layer, depth, receiver, numSamples);

				float area = Coverage(vec2(u_sample_sc,v_sample_sc),x0_mask_sc,y0_mask_sc,x1_mask_sc,y1_mask_sc);
				float f = area*height;

				weight += w * f;
				pcf += s * f;
			}
		}

	}
	else {
		pcf = 1.f;
		weight = 0.f;
	}
	*/
}

// --------------------------------------------------------
// Coverage Filter
// --------------------------------------------------------

// unit pixel size
// assume mask region is larger than pixel size
float Coverage(vec2 uv_sample, float x0_mask, float y0_mask, float x1_mask, float y1_mask) {

	// @params uv_sample : texel center
	// @params x*_mask, y*_mask : mask region

	// pixel is within mask region
	if(uv_sample.x-0.5f>=x0_mask && uv_sample.x+0.5f<=x1_mask && uv_sample.y-0.5f>=y0_mask && uv_sample.y+0.5f<=y1_mask) {
		return 1.f;
	}
	else {
		float ratio_x = 1.f;
		float ratio_y = 1.f;
		if(uv_sample.x-0.5f<x0_mask) {
			ratio_x = uv_sample.x+0.5f-x0_mask;
		}
		if(uv_sample.y-0.5f<y0_mask) {
			ratio_y = uv_sample.y+0.5f-y0_mask;
		}
		if(uv_sample.x+0.5f>x1_mask){
			ratio_x = x1_mask-(uv_sample.x-0.5f);
		}
		if(uv_sample.y+0.5f>y1_mask){
			ratio_y = y1_mask-(uv_sample.y-0.5f);
		}

		return ratio_x * ratio_y;
	}
}

void PCF_Weight_Coverage(out float pcf, out float weight,
	int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

	float invSMSize = 1.f / smSize;
	vec2 uv_sc = uv * smSize;
	
	// x, y: coordinate
	// u, v: texture center

	float x0_mask_sc = uv_sc.x - 0.5f * float(numSamples);
	float x1_mask_sc = uv_sc.x + 0.5f * float(numSamples);
	float y0_mask_sc = uv_sc.y - 0.5f * float(numSamples);
	float y1_mask_sc = uv_sc.y + 0.5f * float(numSamples);

	if(x0_mask_sc >= 0.5f && y0_mask_sc >= 0.5f && x1_mask_sc <= smSize - 0.5f && y1_mask_sc <= smSize - 0.5f) {

		float height = 1.f / float(numSamples * numSamples);

		float x0_sc = floor(x0_mask_sc);
		float y0_sc = floor(y0_mask_sc);
		float x1_sc = floor(x1_mask_sc) + 1.f;
		float y1_sc = floor(y1_mask_sc) + 1.f;

		float u0_sc = x0_sc + 0.5f;
		float v0_sc = y0_sc + 0.5f;

		pcf = 0.f;
		weight = 0.f;
		for(int y = 0; y <= numSamples; ++y) {
			for(int x = 0; x <= numSamples; ++x) {
				float u_sample_sc = u0_sc + x;
				float v_sample_sc = v0_sc + y;

				vec2 uv_sample = vec2(u_sample_sc, v_sample_sc) * invSMSize;
				float depth = LookUpDepth( layer, uv_sample );
				float s = Visible(depth, receiver);
				float w = EstimateLayerWeight(layer, depth, receiver, numSamples);

				float area = Coverage(vec2(u_sample_sc,v_sample_sc),x0_mask_sc,y0_mask_sc,x1_mask_sc,y1_mask_sc);
				float f = area*height;

				weight += w * f;
				pcf += s * f;
			}
		}

	}
	else {
		pcf = 1.f;
		weight = 0.f;
	}
}

// --------------------------------------------------------
// Estimate Visibility Using Random Sampling
// --------------------------------------------------------

float PCF_RandomSampling(int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

	float filterSize = shadowmapFilterSize[layer];

	// shadow of current layer
	float pcf = 0.f;
	float num_samples = 0.f;
	for(int k = 0; k < 16; ++k) {
		vec2 uv_sample = uv + filterSize * RandSample16(k);
		if(uv_sample.x < 0.f || uv_sample.x > 1.f || uv_sample.y < 0.f || uv_sample.y > 1.f)  continue;
		pcf += PCF_BilinearFilter(layer, smSize, uv_sample, uv, receiver, dz_duv, layerRatio);
//		pcf += PCF_BicubicFilter(layer, smSize, uv_sample, uv, receiver, dz_duv, layerRatio);
		num_samples+=1.f;
	}
	pcf /= num_samples;

	return pcf;
}

float Blocker_RandomSampling(int layer, float smSize, vec2 uv, vec2 uv_receiver, float receiver, vec2 dz_duv, float layerRatio) {

	float filterSize = shadowmapFilterSize[layer];

	// blocker of current layer
	float numBlocker = 0.f;		// number of blockers
	float blocker = 0.f;		// average blocker distance
	for(int k = 0; k < 16; ++k) {
		vec2 offset = filterSize * RandSample16(k);
		vec2 uv_sample = uv + offset;
		if (uv_sample.x<=0.0 || uv_sample.x>=1.0 || uv_sample.y<=0.0 || uv_sample.y>=1.0) continue;
		float depth = LookUpDepth( layer, uv_sample );
		float biasedReceiver = receiver; // BiasedZ(receiver, dz_duv, offset, layerRatio);
		if( depth < 1.f && Visible(depth, biasedReceiver) == 0.f ) {
			blocker += depth;
			numBlocker += 1.f;
		}
	}

	// fully lit, ignore the weight of current layer
	if(numBlocker == 0.f) blocker = receiver;
	else blocker /= numBlocker;

	return blocker;
}

float EstimateVisibility_RandomSampling(float receiver, vec2 uv, vec2 dz_duv, float tan_acos_dot_n_l) {

	// for each layer, get the blocker of that layer
	// then compute the weight of that layer

	float visibility = 0.f;
	float weight = 0.f;

	float weights[11];
	float scaled_shadow[11];
	float shadows[11];
	float estimated_r[11];
	float blockers[11];
		
	for(int i = 0; i < numShadowmap; ++i) {
		
		// deal with 2 by 2 texels
		float smSize = shadowmapSize[i];
		float filterSize = shadowmapFilterSize[i];
		float layerRatio = BIAS_CONST_FACTOR * shadowmapSize[0] / smSize;
		float receiver_i = receiver - layerRatio * tan_acos_dot_n_l;

		// shadow of current layer
		float s_i = PCF_RandomSampling(i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
		
		// blocker of current layer

		float blocker = Blocker_RandomSampling(i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);

		blockers[i] = blocker;
		estimated_r[i] = EstimateFilterSize_Cem(receiver, blocker, lightSize);

		// weight
		float w_i = EstimateLayerWeight(i, blocker, receiver_i, numSamples);
		
		visibility += s_i * w_i;
		weight += w_i;

		weights[i] = w_i;
		scaled_shadow[i] = s_i * w_i;
		shadows[i] = s_i;
	}

	switch(displayMode) {
		case 0: {
			visibility += 1.f - weight;
			visibility = clamp(visibility, 0.f, 1.f);	
		}
		break;
		case 1: visibility = weights[layer];
		break;
		case 2: visibility = shadows[layer];
		break;
		case 3: visibility = scaled_shadow[layer];
		break;
		case 4: visibility = weight * 0.5f;
		break;
		case 5:
		break;
		default:
		break;
	}
	
	return visibility;
}

// --------------------------------------------------------
// Estimate Visibility Using Different Filtering
// --------------------------------------------------------

float EstimateVisibility(float receiver, vec2 uv, vec2 dz_duv, float tan_acos_dot_n_l) {

	// for each layer, get the blocker of that layer
	// then compute the weight of that layer

	float s = 0.f;
	float s_w = 0.f;

	float weights[11];
	float scaled_shadow[11];
	float shadows[11];
		
	for(int i = 0; i < numShadowmap; ++i) {

		float smSize = shadowmapSize[i];
		float invSMSize = 1.f / smSize;
		float filterSize = shadowmapFilterSize[i];		
		float layerRatio = BIAS_CONST_FACTOR * shadowmapSize[0] * invSMSize;
		float receiver_i = receiver - layerRatio; // * tan_acos_dot_n_l;

		float s_i = 0.f;
		float w_i = 0.f;
/*
		if(filterType == 1) PCF_Weight_TriangularFilter(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
		else PCF_Weight_GaussianFilter(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
		*/

//		PCF_Weight_TriangularFilter(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
		
//		PCF_Weight_GaussianFilter(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
//		PCF_Weight_BilinearFilter(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
//		PCF_Weight_BicubicFilter(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
		PCF_Weight_Coverage(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
//		PCF_Weight_Pyramidal(s_i, w_i, i, smSize, uv, uv, receiver_i, dz_duv, layerRatio);

		// scaled shadow
		float s_i_sc = s_i * w_i;

		// accumulate visibility and weight
		s += s_i_sc;
		s_w += w_i;

		// store 
		weights[i] = w_i;
		scaled_shadow[i] = s_i_sc;
		shadows[i] = s_i;
	}
	
	switch(displayMode) {
		case 0: {
			s = 1.f - s_w;
			s = clamp(s, 0.f, 1.f);
		}
		break;
		case 1: s = 0.5 * weights[layer];
		break;
//		case 2: s = shadows[layer];
//		break;
//		case 3: s = scaled_shadow[layer];
//		break;
		case 4:	s = 0.5 * s_w;
		break;
		default:
		break;
	}
	
	return s;
}

// --------------------------------------------------------
// Visibility
// --------------------------------------------------------

float Visibility() {
	/*
	Compute the amount of shadows of current fragment.

	@params level: mipmap level of the shadowmap that is used by current fragment.
	@params visibility: visibility of current fragment. 
	*/	

	vec2 uv = DepthUV();					// uv of current fragment in the light space
	float receiver = DistanceToLight();
	
	// adaptive depth bias
	vec3 n = normalize( Normal_cameraspace );
	vec3 l = normalize( LightDirection_cameraspace );
	float dot_n_l = dot(n, l);
	if(dot_n_l < 0.f) dot_n_l = dot(n, -l);
	dot_n_l = clamp(dot_n_l, 0.f, 1.f);
	float tan_acos_dot_n_l = tan( acos(dot_n_l));
	
	vec2 dz_duv = DepthGradient(uv, receiver);
	
	// ------------------------------------------------
	// beyond the light frustrum
	// ------------------------------------------------

	if(uv.x < 0.f || uv.x > 1.f || uv.y < 0.f || uv.y > 1.f || receiver < 0.f || receiver > 1.f) {
		return 1.f;
	}
	
	// ------------------------------------------------
	// visibility
	// ------------------------------------------------

	float visibility = 0.f;
	if(filterType == 0) visibility = EstimateVisibility_RandomSampling(receiver, uv, dz_duv, tan_acos_dot_n_l);
	else visibility = EstimateVisibility(receiver, uv, dz_duv, tan_acos_dot_n_l);


//		float smSize = shadowmapSize[layer];
//		float filterSize = shadowmapFilterSize[layer];
//		float layerRatio = 0.0005f * shadowmapSize[0] / smSize;
//		float receiver_i = receiver - layerRatio;// * tan_acos_dot_n_l;
//		float visibility = Weight_Coverage(layer, smSize, uv, uv, receiver_i, dz_duv, layerRatio);
		
		// uncomment the following to test depth bias

		// 1. take the nearest sample
/*		// coordinate of the nearest texel
		vec2 uv_sc = smSize * uv;
		vec2 uv_sc_round = round(uv_sc);
		vec2 uv_sc_nearest;
		if(uv_sc.x < uv_sc_round.x) uv_sc_nearest.x = uv_sc_round.x - 0.5f;
		else uv_sc_nearest.x = uv_sc_round.x + 0.5f;
		if(uv_sc.y < uv_sc_round.y) uv_sc_nearest.y = uv_sc_round.y - 0.5f;
		else uv_sc_nearest.y = uv_sc_round.y + 0.5f;
		vec2 uv_nearest = uv_sc_nearest / smSize;

		float visibility = 0.f;
		if(uv_nearest.x >= 0.f && uv_nearest.x <= 1.f && uv_nearest.y >= 0.f && uv_nearest.y <= 1.f) {

			float depth = LookUpDepth( layer, uv_nearest );
			float biasedReceiver = receiver_i;
			visibility = Visible(depth, biasedReceiver);
		} // */
		
/*		// 2. take the nearest samples within 2 taps

		float visibility = 0.f;
		float num_samples = 100.f;
		for(int k = 0; k < 100; ++k) {
			vec2 offset = filterSize * RandSample100(k);
			vec2 uv_sample = uv + offset;
			if(uv_sample.x < 0.f || uv_sample.x > 1.f || uv_sample.y < 0.f || uv_sample.y > 1.f) {
				num_samples -= 1.f;
				continue;
			}
//			float depth = LookUpDepth( layer, uv_sample );
//			float biasedReceiver = BiasedZ(receiver_i, dz_duv, offset, layerRatio);
//			float biasedReceiver = BiasedZ(receiver_i, dz_duv, offset, 1.f);
//			visibility += Visible(depth, biasedReceiver);

			visibility += PCF_BilinearFilter(layer, smSize, uv_sample, uv, receiver_i, dz_duv, layerRatio);
		}
		visibility /= num_samples; // */				

	return visibility;
}

// --------------------------------------------------------
// Blinn Phong Shading
// --------------------------------------------------------

vec3 BlinnPhong(float visibility) {

	// Light emission properties
	vec3 LightColor = vec3(1,1,1);
	float LightPower = 1.0f;
	
	// Material properties
	vec3 MaterialDiffuseColor = diffuse;
	vec3 MaterialAmbientColor = 0.f * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3, 0.3, 0.3);

	// Distance to the light
	//float distance = length( LightPosition_worldspace - Position_worldspace );

	// Normal of the computed fragment, in camera space
	vec3 n = normalize( Normal_cameraspace );
	// Direction of the light (from the fragment to the light)
	vec3 l = normalize( LightDirection_cameraspace );
	
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendiular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float NdotL = clamp( dot( n,l ), 0,1 );
	
	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);
	// Direction in which the triangle reflects the light
	vec3 R = reflect(-l,n);
	// Half vector between the light vector and the view vector.
	vec3 H = normalize( E + l );
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float NdotH = clamp( dot( n, H ), 0,1 );

	float specularHardness = 120.f;

	vec3 result = 
		// Ambient : simulates indirect lighting
//		MaterialAmbientColor +
		// Diffuse : "color" of the object
		visibility * MaterialDiffuseColor * LightColor * LightPower * NdotL+
		// Specular : reflective highlight, like a mirror
		visibility * MaterialSpecularColor * LightColor * LightPower * pow(NdotH, specularHardness);

	return result;
}

// --------------------------------------------------------
// Entry Point
// --------------------------------------------------------

void main(){
	
	// Visibility
	float visibility = Visibility();
	
	// Shading
	color = BlinnPhong(visibility);
}