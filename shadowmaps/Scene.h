#pragma once

#ifndef	SCENE_H
#define SCENE_H

// #include <math.h>

#include "Header.h"
#include "cyPoint.h"
typedef cyPoint3f Point3;

#include "cyMatrix3.h"
typedef cyMatrix3f Matrix3;

#include "cyColor.h"
typedef cyColor Color;

const float BIGFLOAT = 1.0e30f;
//const float FLOAT_BIAS = 0.f;
const float FLOAT_BIAS = 0.00001f;
//const float FLOAT_BIAS = 0.001f;
const float FLOAT_BIAS_SHADOW = 0.0001f; // floating bias for shadow ray
//const float FLOAT_BIAS_TRIIT = 0.00001f; // floating bias for triangle intersection
const float FLOAT_BIAS_TRIIT = 0.f; // floating bias for triangle intersection
const float M_PI = 3.1415926f;

/* template<typename, T>
T min(T a, T b) {
	if (a < b) return a;
	else return b;
}

template<typename, Q>
Q max(Q a, Q b) {
	if (a > b) return a;
	else return b;
} */

struct MeshGL {

	GLuint vertexbuffer;
	GLuint uvbuffer;
	GLuint normalbuffer;
	GLuint elementbuffer;
	vector<unsigned short> indices;

	mat4 ModelMatrix;

	vec3 diffuse;
};

struct PinholeCamera{
	vec3 pos;
	vec3 dir;
	vec3 up;
	vec3 right;
	float fov;

	vec3 d_sc;
	vec3 u_sc;
	vec3 v_sc;

	// near and far planes
	float near;
	float far;
};

//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

#include <string.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include <vector>

#include "cyPoint.h"
typedef cyPoint3f Point3;

#include "cyMatrix3.h"
typedef cyMatrix3f Matrix3;

#include "cyColor.h"
typedef cyColor Color;
typedef cyColor24 Color24;

//-------------------------------------------------------------------------------

#ifndef min
# define min(a,b) ((a)<(b)?(a):(b))
#endif

#ifndef max
# define max(a,b) ((a)>(b)?(a):(b))
#endif

#define BIGFLOAT 1.0e30f
#define BIGINTEGER 100000

//-------------------------------------------------------------------------------

class Ray
{
public:
	Point3 p, dir;

	Ray() {}
	Ray(const Point3 &_p, const Point3 &_dir) : p(_p), dir(_dir) {}
	void Normalize() { dir.Normalize(); }
};

//-------------------------------------------------------------------------------

class Box
{
public:
	Point3 pmin, pmax;

	Box() { Init(); }
	Box(const Point3 &_pmin, const Point3 &_pmax) : pmin(_pmin), pmax(_pmax) {}
	Box(float xmin, float ymin, float zmin, float xmax, float ymax, float zmax) : pmin(xmin, ymin, zmin), pmax(xmax, ymax, zmax) {}

	void Init() { pmin.Set(BIGFLOAT, BIGFLOAT, BIGFLOAT); pmax.Set(-BIGFLOAT, -BIGFLOAT, -BIGFLOAT); }

	Point3 Corner(int i)  // 8 corners of the box
	{
		Point3 p;
		p.x = (i & 1) ? pmax.x : pmin.x;
		p.y = (i & 2) ? pmax.y : pmin.y;
		p.z = (i & 4) ? pmax.z : pmin.z;
		return p;
	}

	void operator += (const Point3 &p)
	{
		for (int i = 0; i<3; i++) {
			if (pmin[i] > p[i]) pmin[i] = p[i];
			if (pmax[i] < p[i]) pmax[i] = p[i];
		}
	}
	void operator += (const Box &b)
	{
		for (int i = 0; i<3; i++) {
			if (pmin[i] > b.pmin[i]) pmin[i] = b.pmin[i];
			if (pmax[i] < b.pmax[i]) pmax[i] = b.pmax[i];
		}
	}
	bool IsInside(const Point3 &p) const { for (int i = 0; i<3; i++) if (pmin[i] > p[i] || pmax[i] < p[i]) return false; return true; }
	bool IntersectRay(const Ray &r, float t_max) const;
};

//-------------------------------------------------------------------------------

class Node;

#define HIT_NONE            0
#define HIT_FRONT           1
#define HIT_BACK            2
#define HIT_FRONT_AND_BACK  (HIT_FRONT|HIT_BACK)

struct HitInfo
{
	float z;    // the distance from the ray center to the hit point
	Point3 p;   // position of the hit point
	Point3 N;   // surface normal at the hit point
	const Node *node;   // the object node that was hit
	bool front; // true if the ray hits the front side, false if the ray hits the back side

	HitInfo() : z(BIGFLOAT), node(NULL) {}
};

//-------------------------------------------------------------------------------

// Base class for all object types
class Object
{
public:
	virtual bool IntersectRay(const Ray &ray, HitInfo &hInfo, int hitSide = HIT_FRONT) const = 0;
	virtual void ViewportDisplay() const {} // used for OpenGL display
	virtual Box ComputeBoundBox() = 0;
	virtual Box GetBoundBox() const = 0;

	///
	Object()
	{
		boxExist = false;
		boxNeed = false;
	}

	bool NeedBoundBox() const	{ return boxNeed; }

	///
protected:
	Box boundBox;
	bool boxExist;
	bool boxNeed;
};

//-------------------------------------------------------------------------------

class ItemBase
{
private:
	char *name;                 // The name of the item

public:
	ItemBase() : name(NULL) {}
	virtual ~ItemBase() { if (name) delete[] name; }

	const char* GetName() const { return name ? name : ""; }
	void SetName(const char *newName)
	{
		if (name) delete[] name;
		if (newName) {
			int n = strlen(newName);
			name = new char[n + 1];
			for (int i = 0; i<n; i++) name[i] = newName[i];
			name[n] = '\0';
		}
		else { name = NULL; }
	}

	bool IsNameEqualTo(const char *str) const {
		if (str && name) {
			int n = strlen(name);
			if (n != strlen(str)) return false; // false if the lengths are different
			for (int i = 0; i < n; ++i) {
				if (name[i] != str[i]) return false;
			}
			return true;
		}

		if (!str && !name) return true;

		return false;
	}
};

template <class T> class ItemList : public std::vector<T*>
{
public:
	virtual ~ItemList() { DeleteAll(); }
	void DeleteAll() { int n = size(); for (int i = 0; i<n; i++) if (at(i)) delete at(i); }
};

	//-------------------------------------------------------------------------------

	class Light : public ItemBase
	{
	public:
		virtual Color Illuminate(Point3 p) const = 0;
		virtual Point3 Direction(Point3 p) const = 0;
		virtual bool IsAmbient() const { return false; }
		virtual void SetViewportLight(int lightID) const {} // used for OpenGL display
	};

	class LightList : public ItemList<Light> {};

	//-------------------------------------------------------------------------------

	class Material : public ItemBase
	{
	public:
		virtual Color Shade(const Ray &ray, const HitInfo &hInfo, const LightList &lights, int bounceCount) const = 0;
		virtual void SetViewportMaterial() const {} // used for OpenGL display
	};

	class MaterialList : public ItemList<Material>
	{
	public:
		Material* Find(const char *name) { int n = size(); for (int i = 0; i<n; i++) if (at(i) && strcmp(name, at(i)->GetName()) == 0) return at(i); return NULL; }
	};

	//-------------------------------------------------------------------------------

	class ObjFileInfo : public ItemBase
	{
	private:
		Object *obj;
	public:
		ObjFileInfo() : obj(NULL) {}
		ObjFileInfo(Object *object, const char *name) : obj(object) { SetName(name); }
		~ObjFileInfo() { Delete(); }
		void Delete() { if (obj) delete obj; obj = NULL; }
		void SetObj(Object *object) { Delete(); obj = object; }
		Object* GetObj() { return obj; }
	};

	class ObjFileList : public ItemList<ObjFileInfo>
	{
	public:
		Object* Find(const char *name) { int n = size(); for (int i = 0; i<n; i++) if (at(i) && strcmp(name, at(i)->GetName()) == 0) return at(i)->GetObj(); return NULL; }
	};

	//-------------------------------------------------------------------------------

	class Node : public ItemBase
	{
	private:
		Node **child;               // Child nodes
		int numChild;               // The number of child nodes
		Object *obj;                // Object reference (merely points to the object, but does not own the object, so it doesn't get deleted automatically)
		Material *mtl;              // Material used for shading the object
		Matrix3 tm;                 // Transformation matrix from object space to world space (excluding translation)
		Point3 pos;                 // Translation part of the transformation matrix (position of the node in world space)
		mutable Matrix3 itm;        // Inverse of the transformation matrix (cached)
		mutable bool itmReady;      // Keeps if the inverse transformation matrix is valid
		Box childBoundBox;          // Bounding box of the child nodes, which does not include the object of this node, but includes the objects of the child nodes
		Color diffuse;				// Diffuse color ranges from 0, 1
		Color diffuse255;			// Diffuse color ranges from 0, 255
	public:
		Node() : child(NULL), numChild(0), obj(NULL), mtl(NULL), itmReady(true) { tm.SetIdentity(); itm.SetIdentity(); pos.Zero(); }

		void Init() { DeleteAllChildNodes(); obj = NULL; SetName(NULL); itmReady = true; tm.SetIdentity(); itm.SetIdentity(); pos.Zero(); } // Initialize the node deleting all child nodes

		// Hierarchy management
		int  GetNumChild() const { return numChild; }
		void SetNumChild(int n, int keepOld = false)
		{
			if (n < 0) n = 0;    // just to be sure
			Node **nc = NULL;   // new child pointer
			if (n > 0) nc = new Node*[n];
			for (int i = 0; i<n; i++) nc[i] = NULL;
			if (keepOld) {
				int sn = min(n, numChild);
				for (int i = 0; i<sn; i++) nc[i] = child[i];
			}
			if (child) delete[] child;
			child = nc;
			numChild = n;
		}
		const Node* GetChild(int i) const { return child[i]; }
		Node*       GetChild(int i) { return child[i]; }
		void        SetChild(int i, Node *node) { child[i] = node; }
		void        AppendChild(Node *node) { SetNumChild(numChild + 1, true); SetChild(numChild - 1, node); }
		void        RemoveChild(int i) { for (int j = i; j<numChild - 1; j++) child[j] = child[j - 1]; SetNumChild(numChild - 1); }
		void DeleteAllChildNodes() { for (int i = 0; i<numChild; i++) { child[i]->DeleteAllChildNodes(); delete child[i]; } SetNumChild(0); }

		// Bounding Box
		const Box& ComputeChildBoundBox()
		{
			childBoundBox.Init();
			for (int i = 0; i<numChild; i++) {
				Box childBox = child[i]->ComputeChildBoundBox();
				Object *cobj = child[i]->GetObject();
				if (cobj) childBox += cobj->ComputeBoundBox();
				// transform the box from child coordinates
				Matrix3 ctm = child[i]->GetTransform();
				for (int j = 0; j<8; j++) childBoundBox += ctm * childBox.Corner(j);
			}
			return childBoundBox;
		}
		const Box& GetChildBoundBox() const { return childBoundBox; }

		// Object management
		const Object*   GetObject() const { return obj; }
		Object*         GetObject() { return obj; }
		void            SetObject(Object *object) { obj = object; }

		// Material management
		const Material* GetMaterial() const { return mtl; }
		void            SetMaterial(Material *material) { mtl = material; }

		void			SetDiffuse(Color c) { diffuse = c; }
		const Color&	GetDiffuse() const { return diffuse; }

		void			SetDiffuse255(Color c) { diffuse255 = c; }
		const Color&	GetDiffuse255() const { return diffuse255; }

		// Transformations
		void Translate(Point3 p) { pos += p; }
		void Rotate(Point3 axis, float degree) { Matrix3 m; m.SetRotation(axis, degree*(float)M_PI / 180.0f); Transform(m); }
		void Scale(float sx, float sy, float sz) { Matrix3 m; m.Zero(); m[0] = sx; m[4] = sy; m[8] = sz; Transform(m); }
		void Transform(const Matrix3 &m) { tm *= m; pos = m*pos; itmReady = false; }
		const Point3    GetPosition() const { return pos; }
		const Matrix3&  GetTransform() const { return tm; }
		const Matrix3&  GetInverseTransform() const { if (!itmReady) { tm.GetInverse(itm); itmReady = true; } return itm; }
		Ray ToNodeCoords(const Ray &ray) const
		{
			Ray r;
			GetInverseTransform();  // make sure that itm is valid.
			r.p = itm * (ray.p - pos);
			r.dir = itm * (ray.p + ray.dir - pos) - r.p;
			return r;
		}
		void FromNodeCoords(HitInfo &hInfo) const
		{
			hInfo.p = tm * hInfo.p + pos;
			Point3 N;
			GetInverseTransform();  // make sure that itm is valid.
			N.x = itm.GetColumn(0) % hInfo.N;
			N.y = itm.GetColumn(1) % hInfo.N;
			N.z = itm.GetColumn(2) % hInfo.N;
			hInfo.N = N.GetNormalized();
		}
	};

	//-------------------------------------------------------------------------------

	class Camera
	{
	public:
		Point3 pos, dir, up;
		float fov;
		int imgWidth, imgHeight;
		float nearFrame, farFrame;

		void Init()
		{
			pos.Set(0, 0, 0);
			dir.Set(0, 0, -1);
			up.Set(0, 1, 0);
			fov = 40;
			imgWidth = 200;
			imgHeight = 150;
			nearFrame = 10.0f;
			farFrame = 1000.0f;
		}
	};

	//-------------------------------------------------------------------------------


#endif