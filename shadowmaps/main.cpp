/*
main file.
*/

// Include common headers
#include "Header.h"

// Include the shadow methods
#include "PCSS.h"
#include "VSSM_v2.h"
//#include "Hierarchy.h"
//#include "Hierarchy_v2.h"
#include "Hierarchy_v3.h"
#include "Scene.h"
#include "RayTracer.h"
#include "TriObj.h"
#include "Sphere.h"
#include "Plane.h"
#include <time.h>

// Global variables

GLFWwindow* window;

int window_size_w = getScreenWidth();
int window_size_h = getScreenHeight();

void loadScene(const char * model_file_path, MeshGL &scene) {

	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJ(model_file_path, vertices, uvs, normals);

	std::vector<unsigned short> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);

	// Load it into a VBO

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	scene.elementbuffer = elementbuffer;
	scene.indices = indices;
	scene.normalbuffer = normalbuffer;
	scene.uvbuffer = uvbuffer;
	scene.vertexbuffer = vertexbuffer;
}

// load the scene: planar receiver, planar blocker
void loadScenePlanarReceiverPlanarCloserBlocker(vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {

	// ---------------- Load meshes for opengl ----------------

		{
			// planar receiver (wave)
			MeshGL plane;
			loadScene("../model/plane.obj", plane);
			{
				mat4 scaled = scale(mat4(1.f), vec3(100.f, 100.f, 100.f));
				mat4 rotated = rotate(mat4(1.f), -90.f, vec3(1.f, 0.f, 0.f));
				plane.ModelMatrix = scaled * rotated;
				plane.diffuse = vec3(0.8f, 0.8f, 0.8f);
			}
			sceneGL.push_back(plane);

			// small plane
			MeshGL smallPlane;
			loadScene("../model/plane.obj", smallPlane);
			{
				mat4 rotated = rotate(mat4(1.f), -90.f, vec3(1.f, 0.f, 0.f));
				mat4 translated = translate(mat4(1.f), vec3(1.5f, 2.5f, 1.5f));
				smallPlane.ModelMatrix = translated * rotated;
				smallPlane.diffuse = vec3(49 / 255.f, 130 / 255.f, 189 / 255.f);
			}
			sceneGL.push_back(smallPlane);
		}


	// ---------------- Load meshes for ray tracer ----------------

	{
		// plane object 
		Plane *obj_plane = new Plane;

		Node * node_plane = new Node;
		node_plane->Init();
		node_plane->SetObject(obj_plane);
		node_plane->Rotate(Point3(1.f, 0.f, 0.f), -90.f);
		node_plane->Scale(100.f, 100.f, 100.f);
		node_plane->SetDiffuse(Color(0.8f, 0.8f, 0.8f));
		node_plane->SetDiffuse255(Color(0.8f, 0.8f, 0.8f)*255.f);
		node_plane->SetName("receiver");
		sceneRT.push_back(node_plane);

		// non-planar receiver (wave)
		Node *node_smallPlane = new Node;
		node_smallPlane->Init();
		node_smallPlane->SetObject(obj_plane);
		node_smallPlane->Rotate(Point3(1.f, 0.f, 0.f), -90.f);
		node_smallPlane->Translate(Point3(1.5f, 2.5f, 1.5f));

		node_smallPlane->SetDiffuse(Color(49 / 255.f, 130 / 255.f, 189 / 255.f));
		node_smallPlane->SetDiffuse255(Color(49, 130, 189));
		node_smallPlane->SetName("smallPlane");
		sceneRT.push_back(node_smallPlane);
	}

}

// load the scene: planar receiver, planar blocker
void loadScenePlanarReceiverPlanarBlocker(vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {

	// ---------------- Load meshes for opengl ----------------

		{
			// planar receiver (wave)
			MeshGL plane;
			loadScene("../model/plane.obj", plane);
			{
				mat4 scaled = scale(mat4(1.f), vec3(100.f, 100.f, 100.f));
				mat4 rotated = rotate(mat4(1.f), -90.f, vec3(1.f, 0.f, 0.f));
				plane.ModelMatrix = scaled * rotated;
				plane.diffuse = vec3(0.8f, 0.8f, 0.8f);
			}
			sceneGL.push_back(plane);

			// small plane
			MeshGL smallPlane;
			loadScene("../model/plane.obj", smallPlane);
			{
				mat4 rotated = rotate(mat4(1.f), -90.f, vec3(1.f, 0.f, 0.f));
				mat4 translated = translate(mat4(1.f), vec3(1.5f, 4.5f, 1.5f));
				smallPlane.ModelMatrix = translated * rotated;
				smallPlane.diffuse = vec3(0.8, 0.2, 0.2);
			}
			sceneGL.push_back(smallPlane);
		}


	// ---------------- Load meshes for ray tracer ----------------

	{
		// plane object 
		Plane *obj_plane = new Plane;

		Node * node_plane = new Node;
		node_plane->Init();
		node_plane->SetObject(obj_plane);
		node_plane->Rotate(Point3(1.f, 0.f, 0.f), -90.f);
		node_plane->Scale(100.f, 100.f, 100.f);
		node_plane->SetDiffuse(Color(0.8f, 0.8f, 0.8f));
		node_plane->SetDiffuse255(Color(0.8f, 0.8f, 0.8f)*255.f);
		node_plane->SetName("receiver");
		sceneRT.push_back(node_plane);

		// non-planar receiver (wave)
		Node *node_smallPlane = new Node;
		node_smallPlane->Init();
		node_smallPlane->SetObject(obj_plane);
		node_smallPlane->Rotate(Point3(1.f, 0.f, 0.f), -90.f);
		node_smallPlane->Translate(Point3(1.5f, 4.5f, 1.5f));

		node_smallPlane->SetDiffuse(Color(0.8, 0.2, 0.2));
		node_smallPlane->SetDiffuse255(Color(0.8, 0.2, 0.2)*255.f);
		node_smallPlane->SetName("smallPlane");
		sceneRT.push_back(node_smallPlane);
	}

}

// load the scene: nonplanar receiver, planar blocker
void loadSceneNonplanarReceiverPlanarBlocker(vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {

	// ---------------- Load meshes for opengl ----------------

	{
		// non-planar receiver (wave)
		MeshGL wave;
		loadScene("../model/wave.obj", wave);
		{
			mat4 scaled = scale(mat4(1.f), vec3(0.01f, 0.01f, 0.01f));
			mat4 translated = translate(mat4(1.f), vec3(5.f, 0.f, -2.5f));
			wave.ModelMatrix = translated * scaled;
			wave.diffuse = vec3(0.8f, 0.8f, 0.8f);
		}
		sceneGL.push_back(wave);

		// small plane
		MeshGL smallPlane;
		loadScene("../model/plane.obj", smallPlane);
		{
			mat4 rotated = rotate(mat4(1.f), -90.f, vec3(1.f, 0.f, 0.f));
			mat4 translated = translate(mat4(1.f), vec3(1.5f, 1.5f, 1.5f));
			smallPlane.ModelMatrix = translated * rotated;
			smallPlane.diffuse = vec3(0.8, 0.2, 0.2);
		}
		sceneGL.push_back(smallPlane);
	}


	// ---------------- Load meshes for ray tracer ----------------

	{
		// non-planar receiver (wave)
		// mesh
		TriObj *obj_wave = new TriObj;
		obj_wave->LoadFromFileObj("../model/wave.obj");
		obj_wave->BuildBVH();

		// node
		Node *node_wave = new Node;
		node_wave->Init();
		node_wave->SetObject(obj_wave);
		node_wave->Scale(0.01f, 0.01f, 0.01f);
		node_wave->Translate(Point3(5.f, 0.f, -2.5f));
		node_wave->SetDiffuse(Color(0.8f, 0.8f, 0.8f));
		node_wave->SetDiffuse255(Color(0.8f, 0.8f, 0.8f)*255.f);
		node_wave->SetName("receiver");
		sceneRT.push_back(node_wave);

		// small plane
		Plane *obj_smallPlane = new Plane;

		Node *node_smallPlane = new Node;
		node_smallPlane->Init();
		node_smallPlane->SetObject(obj_smallPlane);
		node_smallPlane->Rotate(Point3(1.f, 0.f, 0.f), -90.f);
		node_smallPlane->Translate(Point3(1.5f, 1.5f, 1.5f));

		node_smallPlane->SetDiffuse(Color(0.8, 0.2, 0.2));
		node_smallPlane->SetDiffuse255(Color(0.8, 0.2, 0.2)*255.f);
		node_smallPlane->SetName("smallPlane");
		sceneRT.push_back(node_smallPlane);
	}
}

// load the scene: planar receiver, box
void loadScenePlanarReceiverBoxBlocker(vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {

	// ---------------- Load meshes for opengl ----------------

		{
			// planar receiver (wave)
			MeshGL plane;
			loadScene("../model/plane.obj", plane);
			{
				mat4 scaled = scale(mat4(1.f), vec3(100.f, 100.f, 100.f));
				mat4 rotated = rotate(mat4(1.f), -90.f, vec3(1.f, 0.f, 0.f));
				plane.ModelMatrix = scaled * rotated;
				plane.diffuse = vec3(0.8f, 0.8f, 0.8f);
			}
			sceneGL.push_back(plane);

			// box 
			MeshGL box;
			loadScene("../model/box.obj", box);
			{
				mat4 scaled = scale(mat4(1.f), vec3(1.f, 2.f, 1.f));
				mat4 translated = translate(mat4(1.f), vec3(1.5f, 0.f, 1.5f));
				box.ModelMatrix = translated * scaled;
				box.diffuse = vec3(31 / 255.f, 120 / 255.f, 180 / 255.f);
			}
			sceneGL.push_back(box);
		}


	// ---------------- Load meshes for ray tracer ----------------

	{
		// plane object 
		Plane *obj_plane = new Plane;

		Node * node_plane = new Node;
		node_plane->Init();
		node_plane->SetObject(obj_plane);
		node_plane->Rotate(Point3(1.f, 0.f, 0.f), -90.f);
		node_plane->Scale(100.f, 100.f, 100.f);
		node_plane->SetDiffuse(Color(0.8f, 0.8f, 0.8f));
		node_plane->SetDiffuse255(Color(0.8f, 0.8f, 0.8f)*255.f);
		node_plane->SetName("receiver");
		sceneRT.push_back(node_plane);

		// box
		TriObj *obj_box = new TriObj;
		obj_box->LoadFromFileObj("../model/box.obj");
		obj_box->BuildBVH();

		Node * node_box = new Node;
		node_box->Init();
		node_box->SetObject(obj_box);
		node_box->Scale(1.f, 2.f, 1.f);
		node_box->Translate(Point3(1.5f, 0.f, 1.5f));
		node_box->SetDiffuse(Color(31 / 255.f, 120 / 255.f, 180 / 255.f));
		node_box->SetDiffuse255(Color(31.f, 120.f, 180.f));
		node_box->SetName("box");
		sceneRT.push_back(node_box);
	}

}

// load the scene: nonplanar receiver, box
void loadSceneNonplanarReceiverBoxBlocker(vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {

	// ---------------- Load meshes for opengl ----------------

		{
			// non-planar receiver (wave)
			MeshGL wave;
			loadScene("../model/wave.obj", wave);
			{
				mat4 scaled = scale(mat4(1.f), vec3(0.01f, 0.01f, 0.01f));
				mat4 translated = translate(mat4(1.f), vec3(5.f, 0.f, -2.5f));
				wave.ModelMatrix = translated * scaled;
				wave.diffuse = vec3(0.8f, 0.8f, 0.8f);
			}
			sceneGL.push_back(wave);

			// box 
			MeshGL box;
			loadScene("../model/box.obj", box);
			{
				mat4 scaled = scale(mat4(1.f), vec3(1.f, 2.f, 1.f));
				mat4 translated = translate(mat4(1.f), vec3(2.5f, -2.5f, 3.5f));
				box.ModelMatrix = translated * scaled;
				box.diffuse = vec3(31 / 255.f, 120 / 255.f, 180 / 255.f);
			}
			sceneGL.push_back(box);
		}


	// ---------------- Load meshes for ray tracer ----------------

	{
		// non-planar receiver (wave)
		// mesh
		TriObj *obj_wave = new TriObj;
		obj_wave->LoadFromFileObj("../model/wave.obj");
		obj_wave->BuildBVH();

		// node
		Node *node_wave = new Node;
		node_wave->Init();
		node_wave->SetObject(obj_wave);
		node_wave->Scale(0.01f, 0.01f, 0.01f);
		node_wave->Translate(Point3(5.f, 0.f, -2.5f));
		node_wave->SetDiffuse(Color(0.8f, 0.8f, 0.8f));
		node_wave->SetDiffuse255(Color(0.8f, 0.8f, 0.8f)*255.f);
		node_wave->SetName("receiver");
		sceneRT.push_back(node_wave);

		// box
		TriObj *obj_box = new TriObj;
		obj_box->LoadFromFileObj("../model/box.obj");
		obj_box->BuildBVH();

		Node * node_box = new Node;
		node_box->Init();
		node_box->SetObject(obj_box);
		node_box->Scale(1.f, 2.f, 1.f);
		node_box->Translate(Point3(2.5f, -2.5f, 3.5f));
		node_box->SetDiffuse(Color(31 / 255.f, 120 / 255.f, 180 / 255.f));
		node_box->SetDiffuse255(Color(31.f, 120.f, 180.f));
		node_box->SetName("box");
		sceneRT.push_back(node_box);
	}
}

// load the scene: planar receiver, bunny
void loadScenePlanarReceiverBunnyBlocker(vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {

	// ---------------- Load meshes for opengl ----------------

		{
			// planar receiver
			MeshGL plane;
			loadScene("../model/plane.obj", plane);
			{
				mat4 scaled = scale(mat4(1.f), vec3(100.f, 100.f, 100.f));
				mat4 rotated = rotate(mat4(1.f), -90.f, vec3(1.f, 0.f, 0.f));
				plane.ModelMatrix = scaled * rotated;
				plane.diffuse = vec3(0.8f, 0.8f, 0.8f);
			}
			sceneGL.push_back(plane);

			// bunny
			MeshGL bunny;
			loadScene("../model/stanford-bunny.obj", bunny);
			{
				mat4 scaled = scale(mat4(1.f), vec3(1.5f, 1.5f, 1.5f));
				mat4 translated = translate(mat4(1.f), vec3(0.f, 0.001f, 0.f));
				bunny.ModelMatrix = translated *scaled;
				bunny.diffuse = vec3(31 / 255.f, 120 / 255.f, 180 / 255.f);
			}
			sceneGL.push_back(bunny);
		}


	// ---------------- Load meshes for ray tracer ----------------

	{
		// plane object 
		Plane *obj_plane = new Plane;

		Node * node_plane = new Node;
		node_plane->Init();
		node_plane->SetObject(obj_plane);
		node_plane->Rotate(Point3(1.f, 0.f, 0.f), -90.f);
		node_plane->Scale(100.f, 100.f, 100.f);
		node_plane->SetDiffuse(Color(0.8f, 0.8f, 0.8f));
		node_plane->SetDiffuse255(Color(0.8f, 0.8f, 0.8f)*255.f);
		node_plane->SetName("receiver");
		sceneRT.push_back(node_plane);

		// bunny
		TriObj *obj_bunny = new TriObj;
		obj_bunny->LoadFromFileObj("../model/stanford-bunny.obj");
		obj_bunny->BuildBVH();

		Node * node_bunny = new Node;
		node_bunny->Init();
		node_bunny->SetObject(obj_bunny);
		node_bunny->Scale(1.5f, 1.5f, 1.5f);
		node_bunny->Translate(Point3(0.f, 0.001f, 0.f));
		node_bunny->SetDiffuse(Color(31 / 255.f, 120 / 255.f, 180 / 255.f));
		node_bunny->SetDiffuse255(Color(31.f, 120.f, 180.f));
		node_bunny->SetName("bunny");
		sceneRT.push_back(node_bunny);
	}
}

// load the scene: nonplanar receiver, bunny
void loadSceneNonplanarReceiverBunnyBlocker(vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {

	// ---------------- Load meshes for opengl ----------------

		{
			// non-planar receiver (wave)
			MeshGL wave;
			loadScene("../model/wave.obj", wave);
			{
				mat4 scaled = scale(mat4(1.f), vec3(0.01f, 0.01f, 0.01f));
				mat4 translated = translate(mat4(1.f), vec3(5.f, 0.f, -2.5f));
				wave.ModelMatrix = translated * scaled;
				wave.diffuse = vec3(0.8f, 0.8f, 0.8f);
			}
			sceneGL.push_back(wave);

			// bunny 
			MeshGL bunny;
			loadScene("../model/stanford-bunny.obj", bunny);
			{
				mat4 scaled = scale(mat4(1.f), vec3(1.5f, 1.5f, 1.5f));
				mat4 translated = translate(mat4(1.f), vec3(0.f, -1.3f, 0.f));
				bunny.ModelMatrix = translated *scaled;
				bunny.diffuse = vec3(31 / 255.f, 120 / 255.f, 180 / 255.f);
			}
			sceneGL.push_back(bunny);
		}


	// ---------------- Load meshes for ray tracer ----------------

	{
		// non-planar receiver (wave)
		// mesh
		TriObj *obj_wave = new TriObj;
		obj_wave->LoadFromFileObj("../model/wave.obj");
		obj_wave->BuildBVH();

		// node
		Node *node_wave = new Node;
		node_wave->Init();
		node_wave->SetObject(obj_wave);
		node_wave->Scale(0.01f, 0.01f, 0.01f);
		node_wave->Translate(Point3(5.f, 0.f, -2.5f));
		node_wave->SetDiffuse(Color(0.8f, 0.8f, 0.8f));
		node_wave->SetDiffuse255(Color(0.8f, 0.8f, 0.8f)*255.f);
		node_wave->SetName("receiver");
		sceneRT.push_back(node_wave);

		// box
		TriObj *obj_bunny = new TriObj;
		obj_bunny->LoadFromFileObj("../model/stanford-bunny.obj");
		obj_bunny->BuildBVH();

		Node * node_bunny = new Node;
		node_bunny->Init();
		node_bunny->SetObject(obj_bunny);
		node_bunny->Scale(1.5f, 1.5f, 1.5f);
		node_bunny->Translate(Point3(0.f, -1.3f, 0.f));
		node_bunny->SetDiffuse(Color(31 / 255.f, 120 / 255.f, 180 / 255.f));
		node_bunny->SetDiffuse255(Color(31.f, 120.f, 180.f));
		node_bunny->SetName("bunny");
		sceneRT.push_back(node_bunny);
	}
}

void loadScene(int sceneID, vector<MeshGL> &sceneGL, vector<Node *> &sceneRT) {
	switch (sceneID) {
	case SCENE::NONPLANA_RRECEIVE_RPLANAR_BLOCKER:
		loadSceneNonplanarReceiverPlanarBlocker(sceneGL, sceneRT);
		break;
	case SCENE::PLANAR_RECEIVER_PLANAR_BLOCKER:
		loadScenePlanarReceiverPlanarBlocker(sceneGL, sceneRT);
		break;
	case SCENE::PLANAR_RECEIVER_BOX_BLOCKER:
		loadScenePlanarReceiverBoxBlocker(sceneGL, sceneRT);
		break;
	case SCENE::NONPLANAR_RECEIVER_BOX_BLOCKER:
		loadSceneNonplanarReceiverBoxBlocker(sceneGL, sceneRT);
		break;
	case SCENE::NONPLANAR_RECEIVER_BUNNY_BLOCKER:
		loadSceneNonplanarReceiverBunnyBlocker(sceneGL, sceneRT);
		break;
	case SCENE::PLANAR_RECEIVER_BUNNY_BLOCKER:
		loadScenePlanarReceiverBunnyBlocker(sceneGL, sceneRT);
		break;
	case SCENE::PLANAR_RECEIVER_PLANAR_CLOSER_BLOCKER:
		loadScenePlanarReceiverPlanarCloserBlocker(sceneGL, sceneRT);
		break;
	default:
		;
	}
}

// fps
#include <time.h>
#include <Windows.h>

// Entry point
int main(void) {

	//initialize random seed
	srand(time(NULL));
	
	// Initialise GLFW
	if (!glfwInit()) {

		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(window_size_w, window_size_h, "Soft Shadow Mip-Maps", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	GLint params_gl_version;
	glGetIntegerv(GL_MAJOR_VERSION, &params_gl_version);
//	cout << "OpenGL Version: " << glGetString(GL_VERSION) << endl;
	cout << "OpenGL Version: " << params_gl_version << endl;

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(window, window_size_w * 0.5f, window_size_h * 0.5f);

	// Black background
	glClearColor(0.0f, 0.0f, 0.f, 0.0f);

	// Blue background
//	glClearColor(166 / 255.f, 206 / 255.f, 227 / 255.f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	// TODO: what's this?
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// ---------------------- shader program: Simple ----------------------

	// Create and compile our GLSL program from the shaders
	GLuint simpleProgramID = LoadShaders("Simple.vertexshader", "Simple.fragmentshader");

	// Get a handle for our "MVP" uniform
	GLuint simpleMatrixID = glGetUniformLocation(simpleProgramID, "MVP");
	// Get a handle for our "rgb" uniform
	GLuint simpleRGBID = glGetUniformLocation(simpleProgramID, "RGB");

	GLuint light_quad_vertexbuffer;
	glGenBuffers(1, &light_quad_vertexbuffer);
	
	// ---------------- Shader programs for shadows ----------------
	Shadow * pcss = new PCSS();
	Shadow * vssm = new VSSM_v2();
	Shadow * hierarchy = new Hierarchy_v3();
//	Shadow * hierarchy = new Hierarchy();
//	Shadow * hierarchy = new Hierarchy_v2();

	std::vector<Shadow *> shadows;
	shadows.push_back(pcss);
	shadows.push_back(vssm);
	shadows.push_back(hierarchy);

	setNumShadowMethods(shadows.size());

	for (std::vector<Shadow *>::iterator v = shadows.begin(); v != shadows.end(); ++v) {
		(*v)->Init();
	}

	// --------- sceneID ---------
//	int sceneID = SCENE::PLANAR_RECEIVER_PLANAR_CLOSER_BLOCKER; //SCENE::PLANAR_RECEIVER_BUNNY_BLOCKER;//:PLANAR_RECEIVER_BOX_BLOCKER;
	int sceneID = SCENE::NONPLANA_RRECEIVE_RPLANAR_BLOCKER; //SCENE::PLANAR_RECEIVER_BUNNY_BLOCKER;//:PLANAR_RECEIVER_BOX_BLOCKER;
	

	// --------- scene configuration for GL and RT ---------
	vector<MeshGL> sceneGL;
	vector<Node *> sceneRT;
	loadScene(sceneID, sceneGL, sceneRT);
	
	// --------- camera & light configuration for the scene ---------
	setCamera(sceneID);
	setLight(sceneID);

	// ---------------- Ray tracer ----------------
	RayTracer * raytracer = new RayTracer(window_size_w, window_size_h);
	
	
	// ---------------------------------------------
	// main loop
	// ---------------------------------------------

	double lastTime = glfwGetTime();
	int nbFrames = 0;
	
	do{		
		// -------------------- Updates ------------------------
		CheckAutoIncreaseFiltersize();
		updateNumberOfSamples();
		updateLayerToShow();

		// Update spot light
		computeSpotLightMatricesFromInputes();

		// Area light quad
		glm::vec3 lightPos = getLightPosition();
		glm::vec3 uniLightRight = glm::normalize(getLightRight());
		glm::vec3 uniLightUp = glm::normalize(getLightUp());
		float lightSize = getLightSize();
		float scaledLightSize = lightSize * 5.f;
		glm::vec3 lightV1 = lightPos + scaledLightSize * uniLightUp - scaledLightSize * uniLightRight;
		glm::vec3 lightV2 = lightPos + scaledLightSize * uniLightUp + scaledLightSize * uniLightRight;
		glm::vec3 lightV3 = lightPos - scaledLightSize * uniLightUp + scaledLightSize * uniLightRight;
		glm::vec3 lightV4 = lightPos - scaledLightSize * uniLightUp - scaledLightSize * uniLightRight;
		GLfloat light_front_quad_vertex_buffer_data[] = {

			lightV1.x, lightV1.y, lightV1.z,
			lightV2.x, lightV2.y, lightV2.z,
			lightV4.x, lightV4.y, lightV4.z,

			lightV4.x, lightV4.y, lightV4.z,
			lightV2.x, lightV2.y, lightV2.z,
			lightV3.x, lightV3.y, lightV3.z
		};
		GLfloat light_back_quad_vertex_buffer_data[] = {

			lightV1.x, lightV1.y, lightV1.z,
			lightV4.x, lightV4.y, lightV4.z,
			lightV2.x, lightV2.y, lightV2.z,

			lightV2.x, lightV2.y, lightV2.z,
			lightV4.x, lightV4.y, lightV4.z,
			lightV3.x, lightV3.y, lightV3.z
		};

		// --------------- Get shadow type ---------------

		const Shadow * shadow = shadows.at(getShadowType());

		// -------------------- fps ------------------------
		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if (currentTime - lastTime >= 1.0){ // If last prinf() was more than 1 sec ago
			static char strFPS[50] = { 0 };
			int shadowType = getShadowType();
			switch (shadowType){
			case 2: {
				// global const filter size
				int filtersize = numberOfSamples();
				// Update which layer to display
				int layer = layerToShow();
				sprintf(strFPS, "Soft Shadow Mip-Maps | FPS: %d | n: %d | layer: %d", nbFrames, filtersize, layer);
			}
				break;
			case 1:{
				sprintf(strFPS, "Variance Soft Shadow Mapping | FPS: %d | Blocker Search: 100 samples", nbFrames);
			}
				break;
			case 0:{
				sprintf(strFPS, "Percentage-Closer Soft Shadow | FPS: %d | Blocker Search: 100 samples | Filtering: 100 samples", nbFrames);
			}
				break;
			}

			glfwSetWindowTitle(glfwGetCurrentContext(), strFPS);

			nbFrames = 0;
			lastTime += 1.0;
		}
		

		// --------------- Render to framebuffer ---------------

		// Compute the MVP matrix from the light's point of view
		// or, for spot light :
		glm::mat4 depthProjectionMatrix = getLightProjectionMatrix();
		glm::mat4 depthViewMatrix = getLightViewMatrix();
		glm::mat4 depthVP = depthProjectionMatrix * depthViewMatrix;

		shadow->RenderToFramebuffer(depthVP, sceneGL);

		// --------------- Render to screen ---------------

		// Compute the MVP matrix from keyboard and mouse input
		computeCameraMatricesFromInputs();
		glm::mat4 ProjectionMatrix = getCameraProjectionMatrix();
		glm::mat4 ViewMatrix = getCameraViewMatrix();
		glm::mat4 VP = ProjectionMatrix * ViewMatrix;

		glm::mat4 biasMatrix(
			0.5, 0.0, 0.0, 0.0,
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0
			);

		glm::vec3 lightInvDir = getLightInvDirection();

		shadow->RenderToScreen(
			window_size_w, window_size_h,
			VP, ViewMatrix, biasMatrix, depthVP,
			lightInvDir,
			lightSize,
			sceneGL);

		// Render the spot light
		glUseProgram(simpleProgramID);
		glm::mat4 MVP = VP;
		glUniformMatrix4fv(simpleMatrixID, 1, GL_FALSE, &MVP[0][0]);

		// draw front quad
		float lightFrontColor[3] = { 1.f, 1.f, 0.f };

		glUniform3fv(simpleRGBID, 1, lightFrontColor);

		// remap the vertex buffer of the light quad
		glBindBuffer(GL_ARRAY_BUFFER, light_quad_vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(light_front_quad_vertex_buffer_data), light_front_quad_vertex_buffer_data, GL_STATIC_DRAW);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, light_quad_vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, 6); // 2*3 indices starting at 0 -> 2 triangles


		// draw back quad
		float lightBackColor[3] = { 0.3f, 0.3f, 0.3f };

		glUniform3fv(simpleRGBID, 1, lightBackColor);

		// remap the vertex buffer of the light quad
		glBufferData(GL_ARRAY_BUFFER, sizeof(light_back_quad_vertex_buffer_data), light_back_quad_vertex_buffer_data, GL_STATIC_DRAW);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, light_quad_vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, 6); // 2*3 indices starting at 0 -> 2 triangles

		glDisableVertexAttribArray(0);

//*		// cast camera ray
		if (doRayTracing()) {
			raytracer->CastCameraRay(sceneRT);
			raytracer->SaveToImage();
			cout << "ray traced image saved!" << endl;
		} //*/

/*		// cast shadow ray
		if (doRayTracing()) {
			raytracer->ShadeScene(VP, ViewMatrix, lightInvDir, sceneGL);
			cout << "scene shaded in GL" << endl;
			raytracer->SaveFrameBufferToImage();
			raytracer->CastShadowRay(sceneRT);
			raytracer->SaveToImage();
			cout << "ray traced shadow saved!" << endl;
		} // */

		// Save current screen if ' ' is pressed
		saveScreenToImage();
		
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(window) == 0);

	// Cleanup VBO and shader
	int n = sceneGL.size();
	for (int i = 0; i < n; ++i) {
		glDeleteBuffers(1, &sceneGL.at(i).vertexbuffer);
		glDeleteBuffers(1, &sceneGL.at(i).uvbuffer);
		glDeleteBuffers(1, &sceneGL.at(i).normalbuffer);
		glDeleteBuffers(1, &sceneGL.at(i).elementbuffer);
	}

	for (std::vector<Shadow *>::iterator v = shadows.begin(); v != shadows.end(); ++v) {
		(*v)->CleanUp();
	}

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}