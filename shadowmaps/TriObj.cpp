//#include "stdafx.h"
#include "TriObj.h"
//#include "Computing.h"
#include "cyBVH.h"

TriObj::TriObj()
{
	boxNeed = true;
	bvhMesh = new cyBVHTriMesh();
	bvh = false;
}

void TriObj::BuildBVH()
{
	bvhMesh->SetMesh(this);
	bvh = true;
}

bool TriObj::IntersectTriMesh(const unsigned int * ids_tri, unsigned int n, const Ray &ray, HitInfo &hInfo, int hitSide) const
{
	bool intersect = false;

	for (int k = 0; k != n; ++k)
	{
		int id = ids_tri[k];

		Point3 faceN = (v[f[id].v[1]] - v[f[id].v[0]]) ^ (v[f[id].v[2]] - v[f[id].v[0]]).GetNormalized();

		if (hitSide == HIT_FRONT)
			if (ray.dir % faceN > -FLOAT_BIAS_TRIIT)
				continue;

		float U, V, T;
		if (IntersectTriangle(ray, f[id], faceN, U, V, T))
		{
			Point3 bc(1.0f - U - V, U, V);
			Point3 N = this->GetNormal(id, bc).GetNormalized();

			if (fabs(N.x) < FLOAT_BIAS && fabs(N.y) < FLOAT_BIAS && fabs(N.z) < FLOAT_BIAS)
				continue;

			if (hitSide == HIT_NONE)
			{
				if (T < hInfo.z + FLOAT_BIAS_TRIIT)
				{
					hInfo.N = N;
					hInfo.p = ray.p + T * ray.dir;
					hInfo.front = (ray.dir % N <= 0.0f);
					hInfo.z = T;
					if (!intersect) intersect = true;
				}
			}
			else if (hitSide == HIT_FRONT)
			{
				if (T < hInfo.z + FLOAT_BIAS_TRIIT && ray.dir % N <= FLOAT_BIAS_TRIIT)
				{
					hInfo.N = N;
					hInfo.p = ray.p + T * ray.dir;
					hInfo.front = true;
					hInfo.z = T;
					if (!intersect) intersect = true;
				}
			}
			/////////
		}

	}// for
	
	return intersect;
}

bool TriObj::TraceBvhNode(unsigned int idx_node, const Ray &ray, HitInfo &hInfo, int hitSide) const
{
	const float *bound = bvhMesh->GetNodeBounds(idx_node);
	Box box(bound[0], bound[1], bound[2], bound[3], bound[4], bound[5]);

	if (!box.IntersectRay(ray, hInfo.z))
		return false;

	if (bvhMesh->IsLeafNode(idx_node))
	{
		bool intersect = IntersectTriMesh(bvhMesh->GetNodeElements(idx_node), bvhMesh->GetNodeElementCount(idx_node), ray, hInfo, hitSide);
		return intersect;
	}
	else
	{
		unsigned int idx_childL, idx_childR;
		bvhMesh->GetChildNodes(idx_node, idx_childL, idx_childR);

		bool intersectL = TraceBvhNode(idx_childL, ray, hInfo, hitSide);
		bool intersectR = TraceBvhNode(idx_childR, ray, hInfo, hitSide);

		return intersectL | intersectR;
	}
}

bool TriObj::IntersectRay(const Ray &ray, HitInfo &hInfo, int hitSide) const
{
//*	// uncomment to use BVH tree as the intersection
	bool intersect = TraceBvhNode(bvhMesh->GetRootNodeID(), ray, hInfo, hitSide);
	return intersect; // */

/*	// uncomment to check triangle intersection without using BVH

	bool intersect = false;

	for (int id = 0; id != nf; ++id)
	{
		Point3 faceN = (v[f[id].v[1]] - v[f[id].v[0]]) ^ (v[f[id].v[2]] - v[f[id].v[0]]).GetNormalized();

		if (hitSide == HIT_FRONT)
			if (ray.dir % faceN > -FLOAT_BIAS_TRIIT)
				continue;

		float U, V, T;
		
		if (IntersectTriangle(ray, f[id], faceN, U, V, T))
		{
			Point3 bc(1.0f - U - V, U, V);
			Point3 N = this->GetNormal(id, bc).GetNormalized();

			if (fabs(N.x) < FLOAT_BIAS_TRIIT && fabs(N.y) < FLOAT_BIAS_TRIIT && fabs(N.z) < FLOAT_BIAS_TRIIT)
				continue;

			if (hitSide == HIT_NONE)
			{
				if (T < hInfo.z)
				{
					hInfo.N = N;
					hInfo.p = ray.p + T * ray.dir;
					hInfo.front = (ray.dir % N <= 0.0f);
					hInfo.z = T;
					if (!intersect) intersect = true;
				}
			}
			else if (hitSide == HIT_FRONT)
			{
				if (T < hInfo.z && ray.dir % N <= FLOAT_BIAS_TRIIT)
				{
					hInfo.N = N;
					hInfo.p = ray.p + T * ray.dir;
					hInfo.front = true;
					hInfo.z = T;
					if (!intersect) intersect = true;
				}
			}
			/////////
		}

	}// for
		
	return intersect;
	
	// */
}

void TriObj::ViewportDisplay() const {}

Box TriObj::ComputeBoundBox()
{
	if (!boxExist)
	{
		boundBox.pmin = v[0];
		boundBox.pmax = v[0];
		for (unsigned int i = 1; i<nv; i++) {
			if (boundBox.pmin.x > v[i].x) boundBox.pmin.x = v[i].x;
			if (boundBox.pmin.y > v[i].y) boundBox.pmin.y = v[i].y;
			if (boundBox.pmin.z > v[i].z) boundBox.pmin.z = v[i].z;
			if (boundBox.pmax.x < v[i].x) boundBox.pmax.x = v[i].x;
			if (boundBox.pmax.y < v[i].y) boundBox.pmax.y = v[i].y;
			if (boundBox.pmax.z < v[i].z) boundBox.pmax.z = v[i].z;
		}
		boxExist = true;
	}

	return boundBox;
}

Box TriObj::GetBoundBox() const
{
	return boundBox;
}

bool TriObj::IntersectTriangle(const Ray &ray, const cyTriFace &face, const Point3 &N, float &U, float &V, float &T) const
{
	Point3 e1 = v[face.v[1]] - v[face.v[0]];
	Point3 e2 = v[face.v[2]] - v[face.v[0]];
	Point3 s = ray.p - v[face.v[0]];

	Point3 p = ray.dir ^ e2;
	Point3 q = s ^ e1;

	float d_tuv = p % e1;
	if (fabs(d_tuv) < FLOAT_BIAS_TRIIT)
		return false;

	T = (q % e2) / d_tuv;
	if (T < 0)
		return false;

	U = (p % s) / d_tuv;
	V = (q % ray.dir) / d_tuv;

	if (U >= 0.0f && V >= 0.0f && U + V <= 1.0f)
	{
		return true;
	}

	return false;
}