#include "Scene.h"

// --------------------------- Box ------------------------------

bool Box::IntersectRay(const Ray &r, float t_max) const
{
	float Min[3] = { this->pmin.x, this->pmin.y, this->pmin.z };
	float Max[3] = { this->pmax.x, this->pmax.y, this->pmax.z };
	float P[3] = { r.p.x, r.p.y, r.p.z };
	float D[3] = { r.dir.x, r.dir.y, r.dir.z };

	float t_near = FLOAT_BIAS;
	float t_far = t_max;

	/// x y z plane
	for (int i = 0; i != 3; ++i)
	{
		if (fabs(D[i]) < FLOAT_BIAS)
			continue;

		float t1 = (Min[i] - P[i]) / D[i];
		float t2 = (Max[i] - P[i]) / D[i];
		if (t1 > t2)
		{
			// swap t1 and t2;
			float temp = t1;
			t1 = t2;
			t2 = temp;
		}

		t_near = max(t1, t_near);
		t_far = min(t2, t_far);
	}

	return t_near < t_far;
}