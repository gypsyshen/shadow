// #include "stdafx.h"
#include "Sphere.h"

Sphere::Sphere()	//有没有调用 BoundObject
{
	boxNeed = false;
}

Box Sphere::ComputeBoundBox()
{
	if (!boxExist)
	{
		boundBox.pmin = Point3(-1.0f, -1.0f, -1.0f);
		boundBox.pmax = Point3(1.0f, 1.0f, 1.0f);
		boxExist = true;
	}
	return boundBox;
}

Box Sphere::GetBoundBox() const
{
	return boundBox;
}

bool Sphere::IntersectRay(const Ray &local_ray, HitInfo &hit, int hitSide) const
{
	float r = 1.f;
	Point3 p_sphere(0.f, 0.f, 0.f);	//the center of the sphere
	Point3 dir_ray = local_ray.dir;	//the direction of the ray
	Point3 p_camera = local_ray.p;	//the camera's position

	float A = dir_ray % dir_ray;
	float B = 2.f * (dir_ray % (p_camera - p_sphere));
	float C = -r*r + (p_camera - p_sphere) % (p_camera - p_sphere);

	float det = B*B - 4.f*A*C;

	bool intersection = false;
	
	if(det >= 0.f) {
		float t_plus = (-B + sqrt(det)) / (2.f*A);
		float t_minus = (-B - sqrt(det)) / (2.f*A);
		if (t_minus > 0.f) {
			hit.p = local_ray.p + t_minus*local_ray.dir;
			hit.N = (hit.p).GetNormalized();
			hit.z = t_minus;
			hit.front = true;
			intersection = true;
		}
		else if (t_plus > 0.f) {
			hit.p = local_ray.p + t_plus*local_ray.dir;
			hit.N = (-hit.p).GetNormalized();
			hit.z = t_plus;
			hit.front = false;
			intersection = true;
		}
	}

	return intersection;
}

void Sphere::ViewportDisplay() const// used for OpenGL display //未实现
{

}