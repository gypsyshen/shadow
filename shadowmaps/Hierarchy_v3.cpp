#include "Hierarchy_v3.h"
#include "Poisson.h"
#include "Scene.h"

// Hierarchy::Hierarchy() {}

Hierarchy_v3::~Hierarchy_v3() {}

void Hierarchy_v3::InitFrameBuffer() {

	// init texture data
	pixelDataOfLayers = new float*[num_shadowmap_mipmap];
	for (int i = 0; i < num_shadowmap_mipmap; ++i) {
		pixelDataOfLayers[i] = new float[shadowmap_size[i] * shadowmap_size[i] * 3];
	}

	/*
	Initialize the frame buffer.
	*/

//	FramebufferName = new GLuint[num_shadowmap_mipmap];
	glGenFramebuffers(1, &FramebufferName);

	// initialize the mipmap
	glGenTextures(1, &depthTextureMipmap);
	glBindTexture(GL_TEXTURE_2D, depthTextureMipmap);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	for (int i = 0; i < num_shadowmap_mipmap; ++i) {
//		glTexImage2D(GL_TEXTURE_2D, i, GL_RGB16F, shadowmap_size[i], shadowmap_size[i], 0, GL_RGB, GL_FLOAT, 0);
		glTexImage2D(GL_TEXTURE_2D, i, GL_RGB32F, shadowmap_size[i], shadowmap_size[i], 0, GL_RGB, GL_FLOAT, pixelDataOfLayers[i]);
	}

//	for (int layer )

	// 
//	depthrenderbuffer = new GLuint[num_shadowmap_mipmap];
	glGenRenderbuffers(1, &depthrenderbuffer);

//	for (int i = 0; i < num_shadowmap_mipmap; ++i) {
		glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

		// ------------------------ depth buffer ------------------------------
		glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, shadowmap_size[0], shadowmap_size[0]);
//		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, shadowmap_size[i], shadowmap_size[i]);
		
		// ---------------- attach the render buffer and color buffer ----------------
		// here color buffer is represented as texuture, which we'll use as the depth maps later
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);
		// Set "depthTexture" as our colour attachement #0
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, depthTextureMipmap, 0);

		// Set the list of draw buffers.
		GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

		// Always check that our framebuffer is ok
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			std::cout << "Hierarchy: frame buffer initialization failed!" << std::endl;
//			return false;
		}
//	}

}

void Hierarchy_v3::InitShadowProgram() {

	/*
	Initialize the shadow program.
	*/

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("ShadowMapping.vertexshader", "ShadowMapping_Hierarchy.fragmentshader");
	
	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");
	DepthBiasID = glGetUniformLocation(programID, "DepthBiasMVP");

	ShadowmapID = glGetUniformLocation(programID, "shadowMap");
	ShadowmapSizeID = glGetUniformLocation(programID, "shadowmapSize");
	ShadowmapFilterSizeID = glGetUniformLocation(programID, "shadowmapFilterSize");

	// Get a handle for our "LightPosition" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");

	// Get a handle for our "poissonDisk" uniform
	poissonDisk16ID = glGetUniformLocation(programID, "poissonDisk16");
	poissonDisk100ID = glGetUniformLocation(programID, "poissonDisk100");
	
	// Get a handle for our "numSamples" uniform
	numSamplesID = glGetUniformLocation(programID, "numSamples");

	// Get a handle for our "layer" uniform
	layerID = glGetUniformLocation(programID, "layer");

	// Get a handle for our "displayMode" uniform
	displayModeID = glGetUniformLocation(programID, "displayMode");

	// Get a handle for our "levelColor" uniform
//	levelColorID = glGetUniformLocation(programID, "levelColor");

	// Get a handle for our "lightSize" uniform
	lightSizeID = glGetUniformLocation(programID, "lightSize");

	diffuseID = glGetUniformLocation(programID, "diffuse");

	filterTypeID = glGetUniformLocation(programID, "filterType");

	numShadowmapID = glGetUniformLocation(programID, "numShadowmap");

	lightFOVID = glGetUniformLocation(programID, "lightFOV");
}

void Hierarchy_v3::InitDepthProgram() {

	/*
	Initialize the depth program.
	*/

	// Create and compile our GLSL program from the shaders
	depthProgramID = LoadShaders("DepthRTT.vertexshader", "DepthRTT.fragmentshader");

	// Get a handle for our "MVP" uniform
	depthMatrixID = glGetUniformLocation(depthProgramID, "depthMVP");
}

void Hierarchy_v3::Init() {

	// shadow map size
	shadowmap_filtersize = nullptr;
	shadowmap_size = nullptr;
	shadowmap_sizef = nullptr;

	// compute num_shadowmap
	num_shadowmap_mipmap = 0;
	num_shadowmap_shader = 0;
	{
		int low_res = getShadowmapSizeLowRes();
		int div = getShadowmapSizeHighRes();

		while (div >= 1) {
			if (div >= low_res) num_shadowmap_shader++;
			num_shadowmap_mipmap++;
			div = div >> 1;
		}
	}

	cout << num_shadowmap_shader << "layers in total." << endl << endl;
	
	// 
	shadowmap_filtersize = new float[num_shadowmap_mipmap];
	shadowmap_size = new int[num_shadowmap_mipmap];
	shadowmap_sizef = new float[num_shadowmap_mipmap];
	
	shadowmap_size[0] = getShadowmapSizeHighRes(); //shadowmap_size_highres;
	shadowmap_sizef[0] = (float)(getShadowmapSizeHighRes());
	for (int i = 1; i < num_shadowmap_mipmap; ++i) {
		int size = shadowmap_size[i - 1] >> 1;
		shadowmap_size[i] = size;
		shadowmap_sizef[i] = (float)size;
	}

	// Initialize the depth program
	InitDepthProgram();

	// Initialize the shadow program
	InitShadowProgram();

	// Initialize the frame buffer
	InitFrameBuffer();
}

// TODO: optimize depthMVPs
void Hierarchy_v3::RenderToFramebuffer(const glm::mat4 &depthVPMatrix,
	const vector<MeshGL> &scene) const {

	/*
	Bind uniforms of the depth program.
	*/
	
//	for (int i = 0; i < num_shadowmap_mipmap; ++i) 
	{
		// render layer 0
		int i = 0;

		glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
		glViewport(0, 0, shadowmap_size[i], shadowmap_size[i]);

		// We don't use bias in the shader, but instead we draw back faces, 
		// which are already separated from the front faces by a small distance 
		// (if your geometry is made this way)
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(depthProgramID);

		int num_model = scene.size();
		for (int k = 0; k < num_model; ++k) {

			const MeshGL &model = scene.at(k);

			glm::mat4 depthMVP = depthVPMatrix * model.ModelMatrix;
			// Send our transformation to the currently bound shader, 
			// in the "MVP" uniform
			glUniformMatrix4fv(depthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);

			// 1rst attribute buffer : vertices
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, model.vertexbuffer);
			glVertexAttribPointer(
				0,  // The attribute we want to configure
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
				);

			// Index buffer
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.elementbuffer);

			// Draw the triangles !
			glDrawElements(
				GL_TRIANGLES,      // mode
				model.indices.size(),    // count
				GL_UNSIGNED_SHORT, // type
				(void*)0           // element array buffer offset
				);

			glDisableVertexAttribArray(0);
		}
	}

	// read pixels of layer 0
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, pixelDataOfLayers[0]);

	// generate depth map data from layer 0: min
	int mode = 0; // 0: min. 1: max. 2: avg.
	glBindTexture(GL_TEXTURE_2D, depthTextureMipmap);
	for (int layer = 1; layer < num_shadowmap_mipmap; ++layer) {
		float * data = pixelDataOfLayers[layer];
		const float * data0 = pixelDataOfLayers[layer-1];

		int res = shadowmap_size[layer];
		int res0 = res << 1;
		for (int y = 0; y < res; ++y) {
			for (int x = 0; x < res; ++x) {
				int x0 = x * 2;
				int x1 = x0 + 1;
				int y0 = y * 2;
				int y1 = y0 + 1;

				float d;

				switch (mode)
				{
				case 0:
				{
					d = data0[y0*res0 * 3 + x0 * 3];
					float d0 = data0[y0*res0 * 3 + x1 * 3];
					if (d > d0) d = d0;

					d0 = data0[y1*res0 * 3 + x0 * 3];
					if (d > d0) d = d0;
					
					d0 = data0[y1*res0 * 3 + x1 * 3];
					if (d > d0) d = d0;
				}
					break;
				case 1:
				{
					d = data0[y0*res0 * 3 + x0 * 3];
					float d0 = data0[y0*res0 * 3 + x1 * 3];
					if (d < d0) d = d0;

					d0 = data0[y1*res0 * 3 + x0 * 3];
					if (d < d0) d = d0;

					d0 = data0[y1*res0 * 3 + x1 * 3];
					if (d < d0) d = d0;
				}
					break;
				case 2:
				{
					d = data0[y0*res0 * 3 + x0 * 3];
					d += data0[y0*res0 * 3 + x1 * 3];
					d += data0[y1*res0 * 3 + x0 * 3];
					d += data0[y1*res0 * 3 + x1 * 3];
					d *= 0.25;
				}
					break;
				default:
					break;
				}
				
				

				data[y * res * 3 + x * 3] = d;
				data[y * res * 3 + x * 3 + 1] = d;
				data[y * res * 3 + x * 3 + 2] = d;
			}
		}

		glTexSubImage2D(GL_TEXTURE_2D, layer, 0, 0, res, res, GL_RGB, GL_FLOAT, data);
	}

}

void Hierarchy_v3::RenderToScreen(int window_size_w, int window_size_h,
	const glm::mat4 &VP, const glm::mat4 &ViewMatrix, const glm::mat4 &biasMatrix, const glm::mat4 &depthVP,
	const glm::vec3 lightInvDir,
	float lightSize,
	const vector<MeshGL> &scene) const {
	
	// get filter size
	int numSamples = numberOfSamples();
	// get the layer to display
	int layer = layerToShow();
	float half_n_samples = 0.5f * (float)numSamples;
	for (int i = 0; i < num_shadowmap_mipmap; ++i) {
		shadowmap_filtersize[i] = half_n_samples / shadowmap_sizef[i];
	}

	/*
	Bind uniforms in the shadow program.
	*/

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, window_size_w, window_size_h); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);


	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	// Bind our shadowmaps in Texture Unit 1
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depthTextureMipmap);
	glUniform1i(ShadowmapID, 1);

	// shadowmap size
	glUniform1fv(ShadowmapSizeID, num_shadowmap_mipmap, shadowmap_sizef);

	// shadowmap filter size
	glUniform1fv(ShadowmapFilterSizeID, num_shadowmap_mipmap, shadowmap_filtersize);

	// poisson samples
	glUniform2fv(poissonDisk16ID, 16, poissonDisk16);
	glUniform2fv(poissonDisk100ID, 100, poissonDisk100);

	glUniform1i(numSamplesID, numSamples);

	glUniform1i(displayModeID, getDisplayMode());
	
	glUniform1f(lightSizeID, lightSize);

	glUniform1i(filterTypeID, getHierarchyFilterType());

	glUniform1i(numShadowmapID, num_shadowmap_shader);

	glUniform1i(layerID, layer);

	glUniform1f(lightFOVID, getLightFoV());

	int num_models = scene.size();
	for (int i = 0; i < num_models; ++i) {

		const MeshGL &model = scene.at(i);
		

		glm::mat4 MVP = VP * model.ModelMatrix;

		glm::mat4 depthMVP = depthVP * model.ModelMatrix;
		glm::mat4 depthBiasMVP = biasMatrix * depthMVP;

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &model.ModelMatrix[0][0]);
		glUniformMatrix4fv(DepthBiasID, 1, GL_FALSE, &depthBiasMVP[0][0]);

		glUniform3f(diffuseID, model.diffuse.r, model.diffuse.g, model.diffuse.b);

		// Draw
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, model.vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, model.uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, model.normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			model.indices.size(),    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
			);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}
	
}

void Hierarchy_v3::CleanUp() {
	
	glDeleteProgram(programID);
	glDeleteProgram(depthProgramID);
}