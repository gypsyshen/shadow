#pragma once

#ifndef SHADOW_HIERARCHY_H
#define SHADOW_HIERARCHY_H

#include "Shadow.h"
//#include "Header.h"

class Hierarchy : public Shadow {
public:

//	Hierarchy();

	~Hierarchy();

	void Init();
	
	void RenderToFramebuffer(const glm::mat4 &depthVPMatrix,
		const vector<MeshGL> &scene) const;

	void RenderToScreen(int window_size_w, int window_size_h,
		const glm::mat4 &VP, const glm::mat4 &ViewMatrix, const glm::mat4 &biasMatrix, const glm::mat4 &depthVP,
		const glm::vec3 lightInvDir,
		float lightSize,
		const vector<MeshGL> &scene) const;


	void CleanUp();

private:

	void InitDepthProgram();
	void InitShadowProgram();
	void InitFrameBuffer();

	// Shadow map size
	int num_shadowmap_mipmap;
	int num_shadowmap_shader;

	float * shadowmap_filtersize;
	int * shadowmap_size;
	float * shadowmap_sizef;

	// Uniforms in depth program
	GLuint depthProgramID;
	GLuint depthMatrixID;

	// Uniforms in the shadow program
	GLuint programID;
	GLuint MatrixID;
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;
	GLuint DepthBiasID;

	GLuint ShadowmapID;
	GLuint ShadowmapSizeID;
	GLuint ShadowmapFilterSizeID;

	GLuint layerID;

	GLint shadowmapUniform;

	GLuint lightInvDirID;
	GLuint lightSizeID;

	GLuint poissonDisk16ID;
	GLuint poissonDisk100ID;

	GLuint numSamplesID;

	GLuint numShadowmapID;

	GLuint displayModeID;
	
	GLuint diffuseID;

	GLuint filterTypeID;

	GLuint lightFOVID;

	// Framebuffer
	GLuint *FramebufferName;
	GLuint *depthrenderbuffer;
	GLuint depthTextureMipmap;
};

#endif