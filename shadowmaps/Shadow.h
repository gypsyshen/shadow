// #pragma once

#ifndef SHADOW_H
#define SHADOW_H

#include "Header.h"
//#include "Scene.h"

class MeshGL;

class Shadow {
public:

	virtual ~Shadow() {}

	virtual void Init() = 0;

	virtual void RenderToFramebuffer(const glm::mat4 &depthVPMatrix, 
		const vector<MeshGL> &scene) const = 0;

	virtual void RenderToScreen(int window_size_w, int window_size_h,
		const glm::mat4 &VP, const glm::mat4 &ViewMatrix, const glm::mat4 &biasMatrix, const glm::mat4 &depthVP,
		const glm::vec3 lightInvDir,
		float lightSize,
		const vector<MeshGL> &scene) const = 0;

	virtual void CleanUp() = 0;
};

#endif