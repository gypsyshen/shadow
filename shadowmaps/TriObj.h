#pragma once
#include "scene.h"
#include "cyTriMesh.h"

class cyBVHTriMesh;

class TriObj : public Object, public cyTriMesh
{
public:
	TriObj();

	bool IntersectRay(const Ray &ray, HitInfo &hInfo, int hitSide = HIT_FRONT) const;
	void ViewportDisplay() const;
	Box ComputeBoundBox();
	Box GetBoundBox() const;

	void BuildBVH();

private:
	bool bvh;
	cyBVHTriMesh * bvhMesh;

	bool TraceBvhNode(unsigned int idx_node, const Ray &ray, HitInfo &hInfo, int hitSide) const;
	bool IntersectTriMesh(const unsigned int * idx_tri, unsigned int n, const Ray &ray, HitInfo &hInfo, int hitSide) const;

	bool IntersectTriangle(const Ray &ray, const cyTriFace &face, const Point3 &N, float &U, float &V, float &T) const;
};