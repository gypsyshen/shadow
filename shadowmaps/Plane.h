#pragma once
#include "scene.h"

class Plane : public Object
{
public:
	Plane();

	bool IntersectRay(const Ray &local_ray, HitInfo &hInfo, int hitSide = HIT_FRONT) const;
	void ViewportDisplay() const; // used for OpenGL display

	Box ComputeBoundBox();
	Box GetBoundBox() const;
};