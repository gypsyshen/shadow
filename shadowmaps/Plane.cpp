// #include "stdafx.h"
#include "Plane.h"

Plane::Plane()
{
	boxNeed = false;
}

Box Plane::ComputeBoundBox()
{
	if (!boxExist)
	{
		boundBox.pmin = Point3(-1.0f, -1.0f, 0.0f);
		boundBox.pmax = Point3(1.0f, 1.0f, 0.0f);
		boxExist = true;
	}
	return boundBox;
}
Box Plane::GetBoundBox() const	//δʵ��
{
	return boundBox;
}

bool Plane::IntersectRay(const Ray &local_ray, HitInfo &hInfo, int hitSide) const
{
	if (fabs(local_ray.dir.z) < FLOAT_BIAS)
		return false;

	float t = -local_ray.p.z / local_ray.dir.z;

	if (t <= 0.0f)	return false;

	Point3 H = local_ray.p + t * local_ray.dir;

	if (H.x >= -1.0f && H.x <= 1.0f)
	{
		if (H.y >= -1.0f && H.y <= 1.0f)
		{
			hInfo.p = H;

			hInfo.N = Point3(0.0f, 0.0f, 1.0f);

			hInfo.z = t;
			hInfo.front = true;
			return true;
		}
	}

	return false;
}

void Plane::ViewportDisplay() const	// used for OpenGL display //δʵ��
{

}