#include "RayTracer.h"
#include <iostream>
#include <ctime>
#include <string.h>
#include <math.h>
#include "Poisson.h"


RayTracer::RayTracer(int width, int height) {
	this->width = width;
	this->height = height;

	this->inv_width = 1.f / float(width);
	this->inv_height = 1.f / float(height);

	data = new unsigned char[3 * width * height];
	depth_buffer = new unsigned char[3 * width * height];

	InitFrameBuffer();
	InitProgram();
}

RayTracer::~RayTracer() {
	if (data) delete data;
}

void RayTracer::InitProgram() {
	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("Shading.vertexshader", "Shading.fragmentshader");

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");

	// Get a handle for our "LightPosition" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");
	
	// Get a handle for our "diffuse" uniform
	diffuseID = glGetUniformLocation(programID, "diffuse");
}

void RayTracer::InitFrameBuffer() {

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// ---------------------- color buffer ----------------------
	// Use color buffer to store depth values.
//	shadeTexture;
	glGenTextures(1, &ColorTexture);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, ColorTexture);

	// Give an empty image to OpenGL ( the last "0" )
	//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, shadowmap_size, shadowmap_size, 0, GL_RGB, GL_FLOAT, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, 0);

	// Poor filtering. Needed !
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// ---------------------- depth buffer ----------------------
	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	//	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, shadowmap_size, shadowmap_size);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	// Set "depthTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ColorTexture, 0);

	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

	// Always check that our framebuffer is ok
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		//	return false;
		std::cout << "PCSS: frame buffer initialization failed!" << std::endl;
	}
}

void RayTracer::CastShadowRay(vector<Node *> &sceneRT) {

	// --------- camera ---------

	PinholeCamera camera;
	// normalized vectors
	cameraConfig(camera.pos, camera.dir, camera.up, camera.right, camera.fov, camera.near, camera.far);

	// scaled u, v and direction
	float v_len = 1.f;
	float u_len = v_len * float(width) * inv_height;
	float d_len = v_len / tan(0.5f*camera.fov / 180.f * M_PI);

	camera.v_sc = -v_len * camera.up;
	camera.u_sc = u_len * camera.right;
	camera.d_sc = d_len * camera.dir;

	// --------- light ---------
	Point3 lightInvDir;
	Point3 lightPos;
	Point3 lightRight;
	Point3 lightUp;
	{
		vec3 invDir = getLightInvDirection();
		lightInvDir = Point3(invDir.x, invDir.y, invDir.z);
		vec3 pos = getLightPosition();
		lightPos = Point3(pos.x, pos.y, pos.z);
		vec3 right = getLightRight();
		lightRight = Point3(right.x, right.y, right.z);
		vec3 up = getLightUp();
		lightUp = Point3(up.x, up.y, up.z);
	}
	float lightSize = 0.5f * getLightSize();
	
	// cast camera rays
	for (int h = 0; h < height; ++h) {
		for (int w = 0; w < width; ++w) {

			// ---- generate camera ray ----

			// pixel center:
			float pixel_w = float(w); // +0.5f;
			float pixel_h = float(h); // +0.5f;

			// scale pixel center to [-1, 1]
			float sc_w = 2.f * float(pixel_w) * inv_width - 1.f;
			float sc_h = 2.f * float(pixel_h) * inv_height - 1.f;

			Ray rayW;
			vec3 p = camera.pos;
			vec3 dir = camera.d_sc + sc_w * camera.u_sc + sc_h * camera.v_sc;

			rayW.p = Point3(p.x, p.y, p.z);
			rayW.dir = Point3(dir.x, dir.y, dir.z);
			rayW.Normalize();

			// 1. trace camera ray
			int n_scene_obj = sceneRT.size();
			HitInfo hit;
			bool intersect = false;
			for (int i = 0; i < n_scene_obj; ++i) {

				const Node * node = sceneRT.at(i);

				// transform ray to each local space
				Ray rayL = node->ToNodeCoords(rayW);
				//				rayL.Normalize();

				// do intersection
				// intersect within hit.z
				HitInfo hit_node;
				bool intersect_node = node->GetObject()->IntersectRay(rayL, hit_node);
				if (intersect_node) {
					node->FromNodeCoords(hit_node);
					if (hit_node.z < hit.z) {
						hit = hit_node;
						hit.node = node;
						intersect = true;
					}
				}
			}

			// 2. compute the visibility for the surface intersecting with the camera ray and towards the light
			int base = h * width * 3 + w * 3;
			float visibility = 1.f;
//			if (intersect && hit.node->IsNameEqualTo("receiver")) {
			if (intersect && hit.N % lightInvDir > 0.f) {
//			if (intersect) {

				int num_samples = 16; // number of samples
				visibility = num_samples;
				for (int k = 0; k < num_samples; ++k) {

					cyPoint2f offset;
					{
//						offset.x = 2.f * (rand() - .5f*RAND_MAX) / RAND_MAX;
//						offset.y = 2.f * (rand() - .5f*RAND_MAX) / RAND_MAX;
						offset.x = rand() / float(RAND_MAX) - .5f;
						offset.y = rand() / float(RAND_MAX) - .5f;
						offset *= lightSize;
					}
					Point3 lightPos_sample = lightPos + offset.x * lightRight + offset.y * lightUp;

					// generate a shadow ray
					Ray shadow_rayW; // (hit.p + FLOAT_BIAS_SHADOW * hit.N, (lightPos_sample - hit.p).GetNormalized());
					{
						Point3 shadow_ray_dir = (lightPos_sample - hit.p).GetNormalized();
						Point3 biased_ray_p = hit.p + FLOAT_BIAS_SHADOW * shadow_ray_dir;
						shadow_rayW.p = biased_ray_p;
						shadow_rayW.dir = shadow_ray_dir;
					}
					// hit surface is blocked if current light sample is blocked by any geometry
					for (int i = 0; i < n_scene_obj; ++i) {

						const Node * node = sceneRT.at(i);

						// transform ray to each local space
						Ray shadow_rayL = node->ToNodeCoords(shadow_rayW);
						//shadow_rayL.Normalize();

						// do intersection
						HitInfo shadow_hit;
						if (node->GetObject()->IntersectRay(shadow_rayL, shadow_hit)) {
							visibility -= 1.f;
							break;
						}
					}
				}

				visibility /= float(num_samples);

//				visibility = 0.f;
			}

			

			// 3. scale the original color with the camera ray
			data[base] = int(visibility * (float)(data[base]));
			data[base + 1] = int(visibility * (float)(data[base + 1]));
			data[base + 2] = int(visibility * (float)(data[base + 2]));

		}
	}


}



Color RayTracer::BlinnPhong(float visibility, const PinholeCamera &camera, const Point3 &lightInvDir, const HitInfo &hit) const {

	// Light emission properties
	Color LightColor = Color(1, 1, 1);
	float LightPower = 1.f;

	// Material properties
	Color MaterialDiffuseColor = hit.node->GetDiffuse();
	Color MaterialAmbientColor = Color(0.1, 0.1, 0.1) * MaterialDiffuseColor;
	Color MaterialSpecularColor = Color(0.3, 0.3, 0.3);

	// Normal of the computed fragment, in camera space
	Point3 n = hit.N.GetNormalized();
	// Direction of the light (from the fragment to the light)
	Point3 l = (Point3(lightInvDir.x, lightInvDir.y, lightInvDir.z)).GetNormalized();
		
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendiular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float NdotL = clamp(n % l, 0.f, 1.f);

	// Eye vector (towards the camera)
	Point3 E = (Point3(camera.pos.x, camera.pos.y, camera.pos.z) - hit.p).GetNormalized();
	// Half vector between the light vector and the view vector.
	Point3 H = (E + l).GetNormalized();
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float NdotH = clamp(n % H, 0.f, 1.f);

	float specularHardness = 120.f;
	Color color =
		// Ambient : simulates indirect lighting
//		MaterialAmbientColor +
		// Diffuse : "color" of the object
		visibility * MaterialDiffuseColor * LightColor * LightPower * NdotL +
		// Specular : reflective highlight, like a mirror
		visibility * MaterialSpecularColor * LightColor * LightPower * pow(NdotH, specularHardness);

	color.r = clamp(color.r, 0.f, 1.f);
	color.g = clamp(color.g, 0.f, 1.f);
	color.b = clamp(color.b, 0.f, 1.f);

	return color;
}

void RayTracer::CastCameraRay(vector<Node *> &sceneRT) {

	// in ray traced scene, every mesh and camera position are scaled by 2

	// 1. camera
	PinholeCamera camera;
	// normalized vectors
	cameraConfig(camera.pos, camera.dir, camera.up, camera.right, camera.fov, camera.near, camera.far);

	// scaled u, v and direction
	float v_len = 1.f;
	float u_len = v_len * float(width) * inv_height;
	float d_len = v_len / tan(0.5f*camera.fov / 180.f * M_PI);

	camera.v_sc = -v_len * camera.up;
	camera.u_sc = u_len * camera.right;
	camera.d_sc = d_len * camera.dir;

	// 2. light
	Point3 lightInvDir;
	Point3 lightPos;
	Point3 lightRight;
	Point3 lightUp;
	{
		vec3 invDir = getLightInvDirection();
		lightInvDir = Point3(invDir.x, invDir.y, invDir.z);
		vec3 pos = getLightPosition();
		lightPos = Point3(pos.x, pos.y, pos.z);
		vec3 right = getLightRight();
		lightRight = Point3(right.x, right.y, right.z);
		vec3 up = getLightUp();
		lightUp = Point3(up.x, up.y, up.z);
	}
	float lightSize = 0.5f * getLightSize();
	
	// the depth map
	float *depth_buffer_F = new float[3*height*width];


	// cast camera rays
	for (int h = 0; h < height; ++h) {
		for (int w = 0; w < width; ++w) {

			// 1. generate ray

			// pixel center:
			float pixel_w = float(w); // +0.5f;
			float pixel_h = float(h); // +0.5f;

			// scale pixel center to [-1, 1]
			float sc_w = 2.f * float(pixel_w) * inv_width - 1.f;
			float sc_h = 2.f * float(pixel_h) * inv_height - 1.f;

			Ray rayW;
			vec3 p = camera.pos;
			vec3 dir = camera.d_sc + sc_w * camera.u_sc + sc_h * camera.v_sc;

			rayW.p = Point3(p.x, p.y, p.z);
			rayW.dir = Point3(dir.x, dir.y, dir.z);
			rayW.Normalize();
			
			// 2. trace ray
			int n_scene_obj = sceneRT.size();
			HitInfo hit;
			bool intersect = false;
			for (int i = 0; i < n_scene_obj; ++i) {

				const Node * node = sceneRT.at(i);

				// transform ray to each local space
				Ray rayL = node->ToNodeCoords(rayW);
				//				rayL.Normalize();

				// do intersection
				// intersect within hit.z
				HitInfo hit_node;
				bool intersect_node = node->GetObject()->IntersectRay(rayL, hit_node);
				if (intersect_node) {
					node->FromNodeCoords(hit_node);
					if (hit_node.z < hit.z) {
						hit = hit_node;
						hit.node = node;
						intersect = true;
					}
				}
			}

			// 3. write to pixel
			int base = h * width * 3 + w * 3;
//			if (intersect && hit.N % lightInvDir >= 0.f) {
			if (intersect) {

				//*				// soft shadow
				float visibility = 0.f;

				int num_samples = 64; // number of samples
				for (int k = 0; k < num_samples; ++k) {

					cyPoint2f offset;
					{
						offset.x = 2.f * (rand() - .5f*RAND_MAX) / float(RAND_MAX);
						offset.y = 2.f * (rand() - .5f*RAND_MAX) / float(RAND_MAX);
						offset *= lightSize;
					}
					Point3 lightPos_sample = lightPos + offset.x * lightRight + offset.y * lightUp;

					// generate a shadow ray
					Ray shadow_rayW(hit.p + FLOAT_BIAS * hit.N, (lightPos_sample - hit.p).GetNormalized());
					// whether the hit surface is lit by current light sample
					float lit = 1.f;
					// hit surface is blocked if current light sample is blocked by any geometry
					for (int i = 0; i < n_scene_obj; ++i) {

						const Node * node = sceneRT.at(i);

						// transform ray to each local space
						Ray shadow_rayL = node->ToNodeCoords(shadow_rayW);
						//shadow_rayL.Normalize();

						// do intersection
						HitInfo shadow_hit;
						if (node->GetObject()->IntersectRay(shadow_rayL, shadow_hit)) {

							lit = 0.f;
							break;
						}
					}
					visibility += lit;
				}

				visibility /= float(num_samples); // */
				
				// shade current point
				Color color = BlinnPhong(visibility, camera, lightInvDir, hit);
				data[base] = int(255.f * color.r);
				data[base + 1] = int(255.f * color.g);
				data[base + 2] = int(255.f * color.b);

				// uncomment to show depth map
				// hit.z should fall between [0, 1]
				depth_buffer_F[base] = hit.z;
				depth_buffer_F[base + 1] = hit.z;
				depth_buffer_F[base + 2] = hit.z;
			}
			// background color
			else {
				data[base] = 0;
				data[base + 1] = 0;
				data[base + 2] = 0;

				depth_buffer_F[base] = BIGFLOAT;
				depth_buffer_F[base + 1] = BIGFLOAT;
				depth_buffer_F[base + 2] = BIGFLOAT;
			}
		}
	}

	// normalize depth_buffer
	{
		float max_depth = 0.f;
		float min_depth = BIGFLOAT;

		for (int i = 0; i < 3 * height * width; ++i) {
			float depthF = depth_buffer_F[i];
			if (depthF == BIGFLOAT) continue;
			if (max_depth < depthF) max_depth = depthF;
			if (min_depth > depthF) min_depth = depthF;
		}

		for (int i = 0; i < 3 * height * width; ++i) {

			float depthF = depth_buffer_F[i];

			if (depthF == BIGFLOAT) depth_buffer[i] = 255;
			else {
				depthF = (depthF - min_depth) / (max_depth - min_depth);
				depth_buffer[i] = (unsigned char)(255.f * depthF);
			}
		}
	}	
}

void RayTracer::SaveToImage() {
		
	// file name: current time
	// current date/time based on current system
	const time_t now = time(0);
	// convert now to string form
	tm *ltm = localtime(&now);

	char filename[50];
	char filename_depth[150];
	float year = ltm->tm_year;
	float month = ltm->tm_mon;
	float day = ltm->tm_mday;
	float hour = ltm->tm_hour;
	float minute = ltm->tm_min;
	float second = ltm->tm_sec;
	sprintf(filename, "../../screenshot/raytraced_%4.f%2.f%2.f-%2.f%2.f%2.f.png", year, month, day, hour, minute, second);
	sprintf(filename_depth, "../../screenshot/raytraced_depth_%4.f%2.f%2.f-%2.f%2.f%2.f.png", year, month, day, hour, minute, second);

	saveBufferToImage(filename, data, width, height);
	saveBufferToImage(filename_depth, depth_buffer, width, height);

}

void RayTracer::SaveFrameBufferToImage() {

	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// 1. read screen pixels
	GLint viewPort[4];
	glGetIntegerv(GL_VIEWPORT, viewPort);
	glReadBuffer(GL_FRONT);
	
	glReadPixels(viewPort[0], viewPort[1], viewPort[2], viewPort[3], GL_RGB, GL_UNSIGNED_BYTE, data);
	int w_mul_3 = width * 3;
	for (int j = 0; j < height; j++) {
		int a = j;
		int b = height - 1 - j;
		if (a >= b) break;
		int base_a = a * w_mul_3;
		int base_b = b * w_mul_3;
		for (int i = 0; i < w_mul_3; i++) {
			int i_a = base_a + i;
			int i_b = base_b + i;
			unsigned char t = data[i_b];
			data[i_b] = data[i_a];
			data[i_a] = t;
		}
	}

	cout << "framebuffer pixels read!" << endl;

	// 3. file name
	// current date/time based on current system
	const time_t now = time(0);
	// convert now to string form
	tm *ltm = localtime(&now);

	char filename[50];
	sprintf(filename, "../../screenshot/raytraced_GLShading.png");

	// 4. store to PNG file
	saveBufferToImage(filename, data, width, height);
}

/*
void RayTracer::CastShadowRay(vector<Node *> &sceneRT) {

// in ray traced scene, every mesh and camera position are scaled by 2

// 1. camera
PinholeCamera camera;
// normalized vectors
cameraConfig(camera.pos, camera.dir, camera.up, camera.right, camera.fov, camera.near, camera.far);

// scaled u, v and direction
float v_len = 1.f;
float u_len = v_len * float(width) * inv_height;
float d_len = v_len / tan(0.5f*camera.fov / 180.f * M_PI);

camera.v_sc = -v_len * camera.up;
camera.u_sc = u_len * camera.right;
camera.d_sc = d_len * camera.dir;

// 2. light
Point3 lightInvDir;
Point3 lightPos;
Point3 lightRight;
Point3 lightUp;
{
vec3 invDir = getLightInvDirection();
lightInvDir = Point3(invDir.x, invDir.y, invDir.z);
vec3 pos = getLightPosition();
lightPos = Point3(pos.x, pos.y, pos.z);
vec3 right = getLightRight();
lightRight = Point3(right.x, right.y, right.z);
vec3 up = getLightUp();
lightUp = Point3(up.x, up.y, up.z);
}
float lightSize = getLightSize();

// the depth map
float *depth_buffer_F = new float[3 * height*width];

// cast camera rays
for (int h = 0; h < height; ++h) {
for (int w = 0; w < width; ++w) {

// 1. generate camera ray

// pixel center:
float pixel_w = float(w); // +0.5f;
float pixel_h = float(h); // +0.5f;

// scale pixel center to [-1, 1]
float sc_w = 2.f * float(pixel_w) * inv_width - 1.f;
float sc_h = 2.f * float(pixel_h) * inv_height - 1.f;

Ray rayW;
vec3 p = camera.pos;
vec3 dir = camera.d_sc + sc_w * camera.u_sc + sc_h * camera.v_sc;

rayW.p = Point3(p.x, p.y, p.z);
rayW.dir = Point3(dir.x, dir.y, dir.z);
rayW.Normalize();

// 2. trace the camera ray
int n_scene_obj = sceneRT.size();
HitInfo hit;
bool intersect = false;
for (int i = 0; i < n_scene_obj; ++i) {

const Node * node = sceneRT.at(i);

// transform ray to each local space
Ray rayL = node->ToNodeCoords(rayW);
//				rayL.Normalize();

// do intersection
// intersect within hit.z
HitInfo hit_node;
bool intersect_node = node->GetObject()->IntersectRay(rayL, hit_node);
if (intersect_node) {
node->FromNodeCoords(hit_node);
if (hit_node.z <= hit.z) {
hit = hit_node;
hit.node = node;
if (!intersect) intersect = true;
}
}
}

// 3. write to pixel
int base = h * width * 3 + w * 3;
// color from GL shadering
Color color = Color(float(data[base]), float(data[base + 1]), float(data[base + 2]));
if (intersect) {
float visibility = 0.f;

// soft shadow
int num_samples = 1; // number of samples
for (int k = 0; k < num_samples; ++k) {

cyPoint2f offset;
{
offset.x = 2.f * (rand() - .5f*RAND_MAX) / RAND_MAX;
offset.y = 2.f * (rand() - .5f*RAND_MAX) / RAND_MAX;
offset *= lightSize;
}
Point3 lightPos_sample = lightPos + offset.x * lightRight + offset.y * lightUp;

// generate a shadow ray
Ray shadow_rayW(hit.p + FLOAT_BIAS * hit.N, (lightPos_sample - hit.p).GetNormalized());
// whether the hit surface is lit by current light sample
float lit = 1.f;
// the hitted surface is blocked if current light sample is blocked by any geometry
for (int i = 0; i < n_scene_obj; ++i) {

const Node * node = sceneRT.at(i);

// transform ray to each local space
Ray shadow_rayL = node->ToNodeCoords(shadow_rayW);
//shadow_rayL.Normalize();

// do intersection
HitInfo shadow_hit;
if (node->GetObject()->IntersectRay(shadow_rayL, shadow_hit)) {
lit = 0.f;
break;
}
}
visibility += lit;
}

visibility /= float(num_samples);

//				if (visibility < 1.f)
{
// shade current point
//					Color color = Color(float(data[base]), float(data[base + 1]), float(data[base + 2]));
color *= visibility;

data[base] = (unsigned char)color.r;
data[base + 1] = (unsigned char)(color.g);
data[base + 2] = (unsigned char)(color.b);
}

depth_buffer_F[base] = hit.z;
depth_buffer_F[base + 1] = hit.z;
depth_buffer_F[base + 2] = hit.z;

} // intersection
else {
depth_buffer_F[base] = BIGFLOAT;
depth_buffer_F[base + 1] = BIGFLOAT;
depth_buffer_F[base + 2] = BIGFLOAT;
}

} // pixel in height
} // pixel in weight

// normalize depth_buffer
{
float max_depth = 0.f;
float min_depth = BIGFLOAT;

for (int i = 0; i < 3 * height * width; ++i) {
float depthF = depth_buffer_F[i];
if (depthF == BIGFLOAT) continue;
if (max_depth < depthF) max_depth = depthF;
if (min_depth > depthF) min_depth = depthF;
}

for (int i = 0; i < 3 * height * width; ++i) {

float depthF = depth_buffer_F[i];

if (depthF == BIGFLOAT) depth_buffer[i] = 255;
else {
depthF = (depthF - min_depth) / (max_depth - min_depth);
depth_buffer[i] = (unsigned char)(255.f * depthF);
}
}
}
}
*/