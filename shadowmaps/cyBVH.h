// cyCodeBase by Cem Yuksel
// [www.cemyuksel.com]
//-------------------------------------------------------------------------------
///
/// \file       cyBVH.h 
/// \author     Cem Yuksel
/// \version    1.0
/// \date       September 23, 2012
///
/// \brief Bounding Volume Hierarchy class.
///
/// cyBVH is a storage class for Bounding Volume Hierarchies.
///

//-------------------------------------------------------------------------------
#include "bvhScene.h"

//-------------------------------------------------------------------------------

#ifndef _CY_BVH_H_INCLUDED_
#define _CY_BVH_H_INCLUDED_

//-------------------------------------------------------------------------------

/// Bounding Volume Hierarchy class

class cyBVH
{
public:

	cyBVH() : nodes(NULL), elements(NULL) {}
	virtual ~cyBVH() { Clear(); }

	/////////////////////////////////////////////////////////////////////////////////
	//@ Node Access Methods
	/////////////////////////////////////////////////////////////////////////////////

	/// Returns the index of the root node.
	unsigned int GetRootNodeID() const { return 1; }

	/// Returns the bounding box of the node.
	const float* GetNodeBounds(unsigned int nodeID) const { return nodes[nodeID].GetBounds(); }

	/// Returns true if the node is a leaf node.
	bool IsLeafNode(unsigned int nodeID) const { return nodes[nodeID].IsLeafNode(); }

	/// Returns the index of the first child node (parent must be an internal node).
	unsigned int GetFirstChildNode(unsigned int parentNodeID) const { return nodes[parentNodeID].ChildIndex(); }

	/// Given the first child node index, returns the index of the second child node.
	unsigned int GetSecondChildNode(unsigned int firstChildNodeID) const { return firstChildNodeID + 1; }

	/// Returns the child nodes of the given node (parent must be an internal node).
	void GetChildNodes(unsigned int parent, unsigned int &child1, unsigned int &child2) const
	{
		child1 = GetFirstChildNode(parent);
		child2 = GetSecondChildNode(child1);
	}

	/// Returns the number of elements inside the given node (must be a leaf node).
	unsigned int GetNodeElementCount(unsigned int nodeID) const  { return nodes[nodeID].ElementCount(); }

	/// Returns the list of element inside the given node (must be a leaf node).
	const unsigned int* GetNodeElements(unsigned int nodeID) const { return &elements[nodes[nodeID].ElementOffset()]; }

	/////////////////////////////////////////////////////////////////////////////////
	//@ Clear and Build Methods
	/////////////////////////////////////////////////////////////////////////////////

	/// Clears the tree structure
	void Clear()
	{
		if (nodes) delete[] nodes;
		nodes = NULL;
		if (elements) delete[] elements;
		elements = NULL;
	}

	/// Builds the tree structure by recursively splitting the nodes from the middle. maxElementsPerNode cannot be larger than 8.
	void Build(unsigned int numElements, unsigned int maxElementsPerNode = CY_BVH_MAX_ELEMENT_COUNT)
	{
		Clear();
		if (numElements == 0) return;
		if (maxElementsPerNode > CY_BVH_MAX_ELEMENT_COUNT) maxElementsPerNode = CY_BVH_MAX_ELEMENT_COUNT;
		elements = new unsigned int[numElements];
		for (unsigned int i = 0; i<numElements; i++) elements[i] = i;
		bvhBox box;
		box.Init();
		for (unsigned int i = 0; i<numElements; i++) {
			bvhBox b;
			GetElementBounds(i, b.b);
			box += b;
		}
		TempNode *tempRoot = new TempNode(numElements, 0, box);
		SplitTempNodeMid(tempRoot, maxElementsPerNode);
		unsigned int numNodes = tempRoot->GetNumNodes();
		nodes = new bvhNode[numNodes + 1];
		ConvertTempData(1, tempRoot, 2);
		delete tempRoot;
	}

	/////////////////////////////////////////////////////////////////////////////////

protected:
	/////////////////////////////////////////////////////////////////////////////////
	//@ Methods to be implemented by sub-classes
	/////////////////////////////////////////////////////////////////////////////////

	virtual void  GetElementBounds(unsigned int i, float box[6]) const = 0;   ///< Sets box as the i^th element's bounding box.
	virtual float GetElementCenter(unsigned int i, int dimension) const = 0;  ///< Returns the center of the i^th element in the given dimension

	/////////////////////////////////////////////////////////////////////////////////

private:

	/////////////////////////////////////////////////////////////////////////////////
	//@ Internal storage
	/////////////////////////////////////////////////////////////////////////////////

	bvhNode            *nodes;     ///< the tree structure that keeps all the node data (nodeData[0] is not used for cache coherency)
	unsigned int    *elements;  ///< indices of all elements in all nodes

	/////////////////////////////////////////////////////////////////////////////////
	//@ Internal methods for building the BVH tree
	/////////////////////////////////////////////////////////////////////////////////   

	void SplitTempNodeMid(TempNode *tNode, unsigned int maxElementsPerNode)
	{
		if (tNode->ElementCount() <= maxElementsPerNode) return;
		const float *box = tNode->GetBounds().b;
		float d[3] = { box[3] - box[0], box[4] - box[1], box[5] - box[2] };
		unsigned int sd[3]; // split dimensions
		sd[0] = d[0] >= d[1] ? (d[0] >= d[2] ? 0 : 2) : (d[1] >= d[2] ? 1 : 2);
		sd[1] = (sd[0] + 1) % 3;
		sd[2] = (sd[0] + 2) % 3;
		if (d[sd[1]] < d[sd[2]]) { int t = sd[1]; sd[1] = sd[2]; sd[2] = t; }

		unsigned int *nodeElements = &elements[tNode->ElementOffset()];
		bvhBox child1Box;
		bvhBox child2Box;
		unsigned int child1ElemCount = 0;
		for (int s = 0; s<3; s++) {
			unsigned int splitDim = sd[s];
			float splitPos = 0.5f * (box[splitDim] + box[splitDim + 3]);
			child1Box.Init();
			child2Box.Init();
			unsigned int i = 0, j = tNode->ElementCount();
			while (i<j) {
				bvhBox eBox;
				GetElementBounds(nodeElements[i], eBox.b);
				float center = GetElementCenter(nodeElements[i], splitDim);
				if (center <= splitPos) {
					i++;
					child1Box += eBox;
				}
				else {
					j--;
					unsigned int t = nodeElements[i];
					nodeElements[i] = nodeElements[j];
					nodeElements[j] = t;
					child2Box += eBox;
				}
			}
			if (i < tNode->ElementCount() && i > 0) {
				child1ElemCount = i;
				break;
			}
		}
		if (child1ElemCount == 0) { // just to be safe
			child1ElemCount = tNode->ElementCount() / 2;
			child1Box.Init();
			for (unsigned int i = 0; i<child1ElemCount; i++) {
				bvhBox eBox;
				GetElementBounds(nodeElements[i], eBox.b);
				child1Box += eBox;
			}
			child2Box.Init();
			for (unsigned int i = child1ElemCount; i<tNode->ElementCount(); i++) {
				bvhBox eBox;
				GetElementBounds(nodeElements[i], eBox.b);
				child2Box += eBox;
			}
		}
		tNode->Split(child1ElemCount, child1Box, child2Box);
		SplitTempNodeMid(tNode->GetChild1(), maxElementsPerNode);
		SplitTempNodeMid(tNode->GetChild2(), maxElementsPerNode);
	}

	unsigned int ConvertTempData(unsigned int nodeID, TempNode *tNode, unsigned int childIndex)
	{
		if (tNode->IsLeafNode()) {
			nodes[nodeID].SetLeafNode(tNode->GetBounds(), tNode->ElementCount(), tNode->ElementOffset());
			return childIndex;
		}
		else {
			nodes[nodeID].SetInternalNode(tNode->GetBounds(), childIndex);
			unsigned int newChildIndex = ConvertTempData(childIndex, tNode->GetChild1(), childIndex + 2);
			return ConvertTempData(childIndex + 1, tNode->GetChild2(), newChildIndex);
		}
	}

	/////////////////////////////////////////////////////////////////////////////////
};

//-------------------------------------------------------------------------------

//#ifdef _CY_TRIMESH_H_INCLUDED_

#include "cyTriMesh.h"

/// BVH hierarchy for triangular meshes (cyTriMesh)

class cyBVHTriMesh : public cyBVH
{
public:
	cyBVHTriMesh() : mesh(NULL) {}
	cyBVHTriMesh(const cyTriMesh *m) { SetMesh(m); }

	/// Sets the mesh pointer and builds the BVH structure.
	void SetMesh(const cyTriMesh *m, unsigned int maxElementsPerNode = CY_BVH_MAX_ELEMENT_COUNT)
	{
		mesh = m;
		Clear();
		Build(mesh->NF(), maxElementsPerNode);
	}

protected:
	/// Sets box as the i^th element's bounding box.
	virtual void GetElementBounds(unsigned int i, float box[6]) const
	{
		const cyTriFace &f = mesh->F(i);
		cyPoint3f p = mesh->V(f.v[0]);
		box[0] = box[3] = p.x; box[1] = box[4] = p.y; box[2] = box[5] = p.z;
		for (int j = 1; j<3; j++) { // for each triangle
			cyPoint3f p = mesh->V(f.v[j]);
			for (int k = 0; k<3; k++) { // for each dimension
				if (box[k] > p[k]) box[k] = p[k];
				if (box[k + 3] < p[k]) box[k + 3] = p[k];
			}
		}
	}

	/// Returns the center of the i^th element in the given dimension
	virtual float GetElementCenter(unsigned int i, int dim) const
	{
		const cyTriFace &f = mesh->F(i);
		return (mesh->V(f.v[0])[dim] + mesh->V(f.v[1])[dim] + mesh->V(f.v[2])[dim]) / 3.0f;
	}

private:
	const cyTriMesh *mesh;
};

//#endif

//-------------------------------------------------------------------------------

#endif