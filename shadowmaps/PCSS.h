#pragma once

#ifndef SHADOW_PCSS_H
#define SHADOW_PCSS_H

#include "Shadow.h"
//#include "Header.h"

class PCSS : public Shadow {
public:

	~PCSS();

	void Init();

	void RenderToFramebuffer(const glm::mat4 &depthVPMatrix, const vector<MeshGL> &scene) const;

	void RenderToScreen(int window_size_w, int window_size_h,
		const glm::mat4 &VP, const glm::mat4 &ViewMatrix, const glm::mat4 &biasMatrix, const glm::mat4 &depthVP,
		const glm::vec3 lightInvDir,
		float lightSize,
		const vector<MeshGL> &scene) const;

	void CleanUp();

private:

	void InitDepthProgram();
	void InitShadowProgram();
	void InitFrameBuffer();

	// IDs in depth program
	GLuint depthProgramID;
	GLuint depthMatrixID;

	// IDs in shadow program
	GLuint programID;

	GLuint MatrixID;
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;
	GLuint DepthBiasID;
	GLuint ShadowMapID;
	GLuint ShadowmapSizeID;

	GLuint diffuseID;

	GLuint lightInvDirID;

	GLuint poissonDisk56ID;
	GLuint poissonDisk100ID;

	GLuint lightSizeID;

	// Shadow map size
	int shadowmap_size;

	// Name of frame buffer & depth texture
	GLuint FramebufferName;
	GLuint depthTexture;
};

#endif