#include "VSSM.h"
#include "Poisson.h"
#include "Scene.h"

VSSM::~VSSM() {}

void VSSM::InitDepthProgram() {
	/*
	Initialize the depth program.
	*/

	// Create and compile our GLSL program from the shaders
	depthProgramID = LoadShaders("DepthRTT.vertexshader", "DepthRTT_VSSM.fragmentshader");

	// Get a handle for our "MVP" uniform
	depthMatrixID = glGetUniformLocation(depthProgramID, "depthMVP");
}

void VSSM::InitShadowProgram() {
	/*
	Initialize the shadow program.
	*/

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("ShadowMapping.vertexshader", "ShadowMapping_VSSM.fragmentshader");
	
	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");
	DepthBiasID = glGetUniformLocation(programID, "DepthBiasMVP");
	ShadowMapID = glGetUniformLocation(programID, "shadowMap");
	ShadowmapSizeID = glGetUniformLocation(programID, "shadowmapSize");

	// Get a handle for our "LightPosition" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");

	// Get a handle for our "poissonDisk" uniform
	poissonDisk100ID = glGetUniformLocation(programID, "poissonDisk100");
	poissonDisk56ID = glGetUniformLocation(programID, "poissonDisk56");

	// Get a handle for our "lightSize" uniform
	lightSizeID = glGetUniformLocation(programID, "lightSize");

	diffuseID = glGetUniformLocation(programID, "diffuse");
}

void VSSM::InitFrameBuffer() {
	/*
	Initialize the frame buffer.
	*/

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// ---------------------- color buffers ----------------------
	// moments: depth, squared depth
	// Use color buffer to store depth values.
	glGenTextures(1, &momentsTexture);
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, momentsTexture);

	// Give an empty image to OpenGL ( the last "0" )
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, shadowmap_size, shadowmap_size, 0, GL_RGB, GL_FLOAT, 0);

	// Poor filtering. Needed !
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	// ---------------------- depth buffer ----------------------
	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, shadowmap_size, shadowmap_size);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	// Set "depthTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, momentsTexture, 0);

	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

	// Always check that our framebuffer is ok
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "VSSM: frame buffer initialization failed!" << std::endl;
//		return false;
	}
}

void VSSM::Init() {

	// Init the shadow map size
	shadowmap_size = 400;

	// Init the depth program
	InitDepthProgram();

	// Init the shadowmap program
	InitShadowProgram();

	// Init the frame buffer
	InitFrameBuffer();
}

void VSSM::RenderToFramebuffer(const glm::mat4 &depthVPMatrix,
	const vector<MeshGL> &scene) const {

	/*
	Bind uniforms of the depth program.
	*/

	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	glViewport(0, 0, shadowmap_size, shadowmap_size); // Render on the whole framebuffer

	// We don't use bias in the shader, but instead we draw back faces, 
	// which are already separated from the front faces by a small distance 
	// (if your geometry is made this way)
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(depthProgramID);
	
	int n = scene.size();
	for (int i = 0; i < n; ++i) {

		const MeshGL &model = scene.at(i);

		glm::mat4 depthMVP = depthVPMatrix * model.ModelMatrix;

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(depthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);

		// Draw the triangles
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, model.vertexbuffer);
		glVertexAttribPointer(
			0,  // The attribute we want to configure
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			model.indices.size(),    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
			);

		glDisableVertexAttribArray(0);
	}
}

void VSSM::RenderToScreen(int window_size_w, int window_size_h,
	const glm::mat4 &VP, const glm::mat4 &ViewMatrix, const glm::mat4 &biasMatrix, const glm::mat4 &depthVP,
	const glm::vec3 lightInvDir,
	float lightSize,
	const vector<MeshGL> &scene) const {

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, window_size_w, window_size_h); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	// Bind our depth texture in Texture Unit 1
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, momentsTexture);
	glUniform1i(ShadowMapID, 1);

	glUniform1f(ShadowmapSizeID, (float)shadowmap_size);

	glUniform2fv(poissonDisk100ID, 100, poissonDisk100);
	glUniform2fv(poissonDisk56ID, 56, poissonDisk56);

	glUniform1f(lightSizeID, lightSize);

	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	int n = scene.size();

	for (int i = 0; i < n; ++i) {

		const MeshGL &model = scene.at(i);

		glm::mat4 MVP = VP * model.ModelMatrix;

		glm::mat4 depthMVP = depthVP * model.ModelMatrix;
		glm::mat4 depthBiasMVP = biasMatrix * depthMVP;

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &model.ModelMatrix[0][0]);
		glUniformMatrix4fv(DepthBiasID, 1, GL_FALSE, &depthBiasMVP[0][0]);

		glUniform3f(diffuseID, model.diffuse.r, model.diffuse.g, model.diffuse.b);

		// Draw
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, model.vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, model.uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, model.normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			model.indices.size(),    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
			);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}
}

void VSSM::CleanUp() {


	glDeleteProgram(programID);
	glDeleteProgram(depthProgramID);

	glDeleteFramebuffers(1, &FramebufferName);
	glDeleteTextures(1, &momentsTexture);
}