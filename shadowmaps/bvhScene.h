#pragma once

//-------------------------------------------
#include "scene.h"

//-------------------------------------------
#define CY_BVH_ELEMENT_COUNT_BITS   3       ///< Determines the maximum number of elements in a node (8)
#define CY_BVH_MAX_ELEMENT_COUNT    (1<<CY_BVH_ELEMENT_COUNT_BITS)
#define CY_BVH_NODE_DATA_BITS       (sizeof(unsigned int)*8)
#define CY_BVH_ELEMENT_COUNT_MASK   ((1<<CY_BVH_ELEMENT_COUNT_BITS)-1)
#define CY_BVH_LEAF_BIT_MASK        ((unsigned int)1<<(CY_BVH_NODE_DATA_BITS-1))
#define CY_BVH_CHILD_INDEX_BITS     (CY_BVH_NODE_DATA_BITS-1)
#define CY_BVH_CHILD_INDEX_MASK     (CY_BVH_LEAF_BIT_MASK-1)
#define CY_BVH_ELEMENT_OFFSET_BITS  (CY_BVH_NODE_DATA_BITS-1-CY_BVH_ELEMENT_COUNT_BITS)
#define CY_BVH_ELEMENT_OFFSET_MASK  ((1<<CY_BVH_ELEMENT_OFFSET_BITS)-1)

//-------------------------------------------
class bvhBox//: public Box
{
public:
        float b[6];
        bvhBox() {}
        bvhBox(const bvhBox &bvh_box) { for(int i=0; i<6; i++) b[i]=bvh_box.b[i]; }
        void Init() { b[0]=b[1]=b[2]=BIGFLOAT; b[3]=b[4]=b[5]=-BIGFLOAT; }
        void operator += (const bvhBox &bvh_box) 
		{
			for(int i=0; i<3; i++) 
			{
				if(b[i]>bvh_box.b[i])
					b[i]=bvh_box.b[i]; 
				if(b[i+3]<bvh_box.b[i+3])
					b[i+3]=bvh_box.b[i+3]; 
			}
		}
};

class bvhNode//: public Node
{
public:
    void SetLeafNode( const bvhBox &bound, unsigned int elemCount, unsigned int elemOffset ) 
	{ 
		box=bound; 
		data=(elemOffset&CY_BVH_ELEMENT_OFFSET_MASK)|((elemCount-1)<<CY_BVH_ELEMENT_OFFSET_BITS)|CY_BVH_LEAF_BIT_MASK; 
	}
    void SetInternalNode( const bvhBox &bound, unsigned int chilIndex ) 
	{
		box=bound; 
		data=(chilIndex&CY_BVH_CHILD_INDEX_MASK); 
	}
    unsigned int    ChildIndex()    const { return (data&CY_BVH_CHILD_INDEX_MASK); }                                    ///< returns the index to the first child (must be internal node)
    unsigned int    ElementOffset() const { return (data&CY_BVH_ELEMENT_OFFSET_MASK); }                                 ///< returns the offset to the first element (must be leaf node)
    unsigned int    ElementCount()  const { return ((data>>CY_BVH_ELEMENT_OFFSET_BITS)&CY_BVH_ELEMENT_COUNT_MASK)+1; }    ///< returns the number of elements in this node (must be leaf node)
    bool            IsLeafNode()    const { return (data&CY_BVH_LEAF_BIT_MASK)>0; }                                      ///< returns true if this is a leaf node
    const float*    GetBounds()     const { return box.b; }                                                             ///< returns the bounding box of the node
private:
    bvhBox             box;    ///< bounding box of the node
    unsigned int    data;   ///< node data bits that keep the leaf node flag and the child node index or element count and element offset.
};

/// Used for building the hierarchy and then converted to NodeData
class TempNode
{
public:
    TempNode( unsigned int count, unsigned int offset, const bvhBox &boundBox) : child1(NULL), child2(NULL), elementCount(count), elementOffset(offset), box(boundBox) {}
    ~TempNode() { if ( child1 ) delete child1; if ( child2 ) delete child2; }
 
    void Split( unsigned int child1ElementCount, const bvhBox &child1Box, const bvhBox &child2Box )
    {
        child1 = new TempNode(child1ElementCount,elementOffset,child1Box);
        child2 = new TempNode(ElementCount()-child1ElementCount,elementOffset+child1ElementCount,child2Box);
    }
    unsigned int GetNumNodes() const
    {
        unsigned int n = 1;
        if ( child1 ) n += child1->GetNumNodes();
        if ( child2 ) n += child2->GetNumNodes();
        return n;
    }
    bool IsLeafNode() const { return child1==NULL; }
    unsigned int ElementCount() const { return elementCount; }
    unsigned int ElementOffset() const { return elementOffset; }
    TempNode* GetChild1() { return child1; }
    TempNode* GetChild2() { return child2; }
    const bvhBox& GetBounds() const { return box; }

private:
    TempNode        *child1, *child2;
    bvhBox             box;
    unsigned int    elementCount;
    unsigned int    elementOffset;
};