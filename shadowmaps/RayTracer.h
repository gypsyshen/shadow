#pragma once

#ifndef SHADOW_RAYTRACER_H
#define SHADOW_RAYTRACER_H

#include "Header.h"
#include "Scene.h"
#include "TriObj.h"

class RayTracer {

private:
	~RayTracer();

	unsigned char * data; // RGB
	unsigned char * depth_buffer; // ray depths

	int width, height;
	float inv_width, inv_height;

	// framebuffer name
	GLuint FramebufferName;
	GLuint ColorTexture;

	// IDs in shading program
	GLuint programID;

	GLuint MatrixID;
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;

	GLuint diffuseID;

	GLuint lightInvDirID;

public:

	RayTracer(int width, int height);

	void SaveToImage();
	void CastCameraRay(vector<Node *> &sceneRT);

	void CastShadowRay(vector<Node *> &sceneRT);

//	void ShadeScene(const mat4 &VP, const mat4 &ViewMatrix, const vec3 lightInvDir, const vector<MeshGL> &sceneGL) {}

//	void ShadeScene(const mat4 &VP, const mat4 &ViewMatrix, const vec3 lightInvDir, const vector<MeshGL> &sceneGL);

	void SaveFrameBufferToImage();

private:
	Color BlinnPhong(float visibility, const PinholeCamera &camera, const Point3 &lightInvDir, const HitInfo &hit) const;

	void InitFrameBuffer();
	void InitProgram();

public:
	
	void ShadeScene(const mat4 &VP, const mat4 &ViewMatrix, const vec3 lightInvDir, const vector<MeshGL> &sceneGL) {

		glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
		glViewport(0, 0, width, height); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		int n = sceneGL.size();

		for (int i = 0; i < n; ++i) {

			const MeshGL &model = sceneGL.at(i);

			glm::mat4 MVP = VP * model.ModelMatrix;

			// Send our transformation to the currently bound shader,
			// in the "MVP" uniform
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
			glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
			glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &model.ModelMatrix[0][0]);

			glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

			glUniform3f(diffuseID, model.diffuse.r, model.diffuse.g, model.diffuse.b);

			// Draw
			// 1rst attribute buffer : vertices
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, model.vertexbuffer);
			glVertexAttribPointer(
				0,                  // attribute
				3,                  // size
				GL_FLOAT,           // type
				GL_FALSE,           // normalized?
				0,                  // stride
				(void*)0            // array buffer offset
				);

			// 2nd attribute buffer : UVs
			glEnableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, model.uvbuffer);
			glVertexAttribPointer(
				1,                                // attribute
				2,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
				);

			// 3rd attribute buffer : normals
			glEnableVertexAttribArray(2);
			glBindBuffer(GL_ARRAY_BUFFER, model.normalbuffer);
			glVertexAttribPointer(
				2,                                // attribute
				3,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
				);

			// Index buffer
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.elementbuffer);

			// Draw the triangles !
			glDrawElements(
				GL_TRIANGLES,      // mode
				model.indices.size(),    // count
				GL_UNSIGNED_SHORT, // type
				(void*)0           // element array buffer offset
				);

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glDisableVertexAttribArray(2);
		}
	}
};


#endif