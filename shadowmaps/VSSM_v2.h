#pragma once

#ifndef SHADOW_VSSM_v2_H
#define SHADOW_VSSM_v2_H

#include "Shadow.h"

class VSSM_v2 : public Shadow {
public:

	~VSSM_v2();

	void Init();

	void RenderToFramebuffer(const glm::mat4 &depthVPMatrix,
		const vector<MeshGL> &scene) const;

	void RenderToScreen(int window_size_w, int window_size_h,
		const glm::mat4 &VP, const glm::mat4 &ViewMatrix, const glm::mat4 &biasMatrix, const glm::mat4 &depthVP,
		const glm::vec3 lightInvDir,
		float lightSize,
		const vector<MeshGL> &scene) const;

	void CleanUp();

private:

	void InitDepthProgram();
	void InitShadowProgram();
	void InitFrameBuffer();

	// Framebuffer and the texture that holds the moments
	GLuint FramebufferName;
	GLuint momentsTexture;

	// Uniforms for the shadow program
	GLuint programID;
	GLuint MatrixID;
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;
	GLuint DepthBiasID;
	GLuint ShadowMapID;
	GLuint ShadowmapSizeID;
	GLuint lightInvDirID;
	GLuint poissonDisk100ID;
	GLuint poissonDisk56ID;
	GLuint lightSizeID;

	// Uniforms for the depth program
	GLuint depthProgramID;
	GLuint depthMatrixID;

	// Uniform for diffuse
	GLuint diffuseID;

	// Shadow map size
	int shadowmap_size;
};

#endif