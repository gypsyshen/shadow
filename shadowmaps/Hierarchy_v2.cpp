#include "Hierarchy_v2.h"
#include "Poisson.h"
#include "Scene.h"

// Hierarchy::Hierarchy() {}

Hierarchy_v2::~Hierarchy_v2() {}

void Hierarchy_v2::InitFrameBuffer() {
	/*
	Initialize the frame buffer.
	*/

	glGenFramebuffers(1, &FramebufferName);

	// initialize the mipmap
	glGenTextures(1, &depthTextureMipmap);
	glBindTexture(GL_TEXTURE_2D, depthTextureMipmap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, shadowmap_size[0], shadowmap_size[0], 0, GL_RGB, GL_FLOAT, 0);

	// 
	glGenRenderbuffers(1, &depthrenderbuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// ------------------------ depth buffer ------------------------------
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, shadowmap_size[0], shadowmap_size[0]);
		
	// ---------------- attach the render buffer and color buffer ----------------
	// here color buffer is represented as texuture, which we'll use as the depth maps later
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);
	// Set "depthTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, depthTextureMipmap, 0);

	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

	// Always check that our framebuffer is ok
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "Hierarchy: frame buffer initialization failed!" << std::endl;
//		return false;
	}

}

void Hierarchy_v2::InitShadowProgram() {

	/*
	Initialize the shadow program.
	*/

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("ShadowMapping.vertexshader", "ShadowMapping_Hierarchy_v2.fragmentshader");
	
	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");
	DepthBiasID = glGetUniformLocation(programID, "DepthBiasMVP");

	ShadowmapID = glGetUniformLocation(programID, "shadowMap");
	ShadowmapSizeID = glGetUniformLocation(programID, "shadowmapSize");
	ShadowmapFilterSizeID = glGetUniformLocation(programID, "shadowmapFilterSize");

	// Get a handle for our "LightPosition" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");

	// Get a handle for our "poissonDisk" uniform
	poissonDisk16ID = glGetUniformLocation(programID, "poissonDisk16");
	poissonDisk100ID = glGetUniformLocation(programID, "poissonDisk100");
	
	// Get a handle for our "numSamples" uniform
	numSamplesID = glGetUniformLocation(programID, "numSamples");

	// Get a handle for our "layer" uniform
	layerID = glGetUniformLocation(programID, "layer");

	// Get a handle for our "displayMode" uniform
	displayModeID = glGetUniformLocation(programID, "displayMode");
	
	// Get a handle for our "lightSize" uniform
	lightSizeID = glGetUniformLocation(programID, "lightSize");

	diffuseID = glGetUniformLocation(programID, "diffuse");

	filterTypeID = glGetUniformLocation(programID, "filterType");

	numShadowmapID = glGetUniformLocation(programID, "numShadowmap");

	lightFOVID = glGetUniformLocation(programID, "lightFOV");
}

void Hierarchy_v2::InitDepthProgram() {

	/*
	Initialize the depth program.
	*/

	// Create and compile our GLSL program from the shaders
	depthProgramID = LoadShaders("DepthRTT.vertexshader", "DepthRTT.fragmentshader");

	// Get a handle for our "MVP" uniform
	depthMatrixID = glGetUniformLocation(depthProgramID, "depthMVP");
}

void Hierarchy_v2::Init() {

	// shadow map size
	shadowmap_filtersize = nullptr;
	shadowmap_size = nullptr;
	shadowmap_sizef = nullptr;

	// compute num_shadowmap
	num_shadowmap_mipmap = 0;
	num_shadowmap_shader = 0;
	{
		int low_res = getShadowmapSizeLowRes();
		int div = getShadowmapSizeHighRes();

		while (div >= 1) {
			if (div >= low_res) num_shadowmap_shader++;
			num_shadowmap_mipmap++;
			div = div >> 1;
		}
	}

	cout << num_shadowmap_shader << "layers in total." << endl << endl;
	
	// 
	shadowmap_filtersize = new float[num_shadowmap_mipmap];
	shadowmap_size = new int[num_shadowmap_mipmap];
	shadowmap_sizef = new float[num_shadowmap_mipmap];
	
	shadowmap_size[0] = getShadowmapSizeHighRes(); //shadowmap_size_highres;
	shadowmap_sizef[0] = (float)(getShadowmapSizeHighRes());
	for (int i = 1; i < num_shadowmap_mipmap; ++i) {
		int size = shadowmap_size[i - 1] >> 1;
		shadowmap_size[i] = size;
		shadowmap_sizef[i] = (float)size;
	}

	// Initialize the depth program
	InitDepthProgram();

	// Initialize the shadow program
	InitShadowProgram();

	// Initialize the frame buffer
	InitFrameBuffer();
}

// TODO: optimize depthMVPs
void Hierarchy_v2::RenderToFramebuffer(const glm::mat4 &depthVPMatrix,
	const vector<MeshGL> &scene) const {

	/*
	Bind uniforms of the depth program.
	*/
	
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	glViewport(0, 0, shadowmap_size[0], shadowmap_size[0]);

	// We don't use bias in the shader, but instead we draw back faces, 
	// which are already separated from the front faces by a small distance 
	// (if your geometry is made this way)
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(depthProgramID);

	int num_model = scene.size();
	for (int k = 0; k < num_model; ++k) {

		const MeshGL &model = scene.at(k);

		glm::mat4 depthMVP = depthVPMatrix * model.ModelMatrix;
		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(depthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, model.vertexbuffer);
		glVertexAttribPointer(
			0,  // The attribute we want to configure
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			model.indices.size(),    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
		);

		glDisableVertexAttribArray(0);
	}
}

void Hierarchy_v2::RenderToScreen(int window_size_w, int window_size_h,
	const glm::mat4 &VP, const glm::mat4 &ViewMatrix, const glm::mat4 &biasMatrix, const glm::mat4 &depthVP,
	const glm::vec3 lightInvDir,
	float lightSize,
	const vector<MeshGL> &scene) const {
	
	// Update number of samples for filtering
	int numSamples = numberOfSamples();
	// Update which layer to display
	int layer = layerToShow();
	for (int i = 0; i < num_shadowmap_mipmap; ++i) {
		shadowmap_filtersize[i] = 0.5f * (float)numSamples / shadowmap_sizef[i];
	}

	/*
	Bind uniforms in the shadow program.
	*/

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, window_size_w, window_size_h); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);


	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	// Bind our shadowmaps in Texture Unit 1
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depthTextureMipmap);
	glUniform1i(ShadowmapID, 1);

	// shadowmap size
	glUniform1fv(ShadowmapSizeID, num_shadowmap_mipmap, shadowmap_sizef);

	// shadowmap filter size
	glUniform1fv(ShadowmapFilterSizeID, num_shadowmap_mipmap, shadowmap_filtersize);

	// poisson samples
	glUniform2fv(poissonDisk16ID, 16, poissonDisk16);
	glUniform2fv(poissonDisk100ID, 100, poissonDisk100);

	glUniform1i(numSamplesID, numSamples);

	glUniform1i(displayModeID, getDisplayMode());
	
//	glUniform3fv(levelColorID, num_shadowmap, levelColor);

	glUniform1f(lightSizeID, lightSize);

	glUniform1i(filterTypeID, getHierarchyFilterType());

	glUniform1i(numShadowmapID, num_shadowmap_shader);

	glUniform1i(layerID, layer);

	glUniform1f(lightFOVID, getLightFoV());

	int num_models = scene.size();
	for (int i = 0; i < num_models; ++i) {

		const MeshGL &model = scene.at(i);
		

		glm::mat4 MVP = VP * model.ModelMatrix;

		glm::mat4 depthMVP = depthVP * model.ModelMatrix;
		glm::mat4 depthBiasMVP = biasMatrix * depthMVP;

		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &model.ModelMatrix[0][0]);
		glUniformMatrix4fv(DepthBiasID, 1, GL_FALSE, &depthBiasMVP[0][0]);

		glUniform3f(diffuseID, model.diffuse.r, model.diffuse.g, model.diffuse.b);

		// Draw
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, model.vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, model.uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, model.normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			model.indices.size(),    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
			);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}
	
}

void Hierarchy_v2::CleanUp() {
	
	glDeleteProgram(programID);
	glDeleteProgram(depthProgramID);

	glDeleteFramebuffers(1, &FramebufferName);
	glDeleteRenderbuffers(1, &depthrenderbuffer);
//	delete FramebufferName;
//	delete depthrenderbuffer;
}