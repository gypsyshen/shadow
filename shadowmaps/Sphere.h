#pragma once
#include "scene.h"

class Sphere : public Object
{
public:
	Sphere();

	bool IntersectRay(const Ray &ray, HitInfo &hInfo, int hitSide = HIT_FRONT) const;
	void ViewportDisplay() const;
	Box ComputeBoundBox();
	Box GetBoundBox() const;
};