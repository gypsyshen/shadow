#ifndef PNG_HPP
#define PNG_HPP

#include <fstream>

unsigned lodepng_save_file(const unsigned char* buffer, size_t buffersize, const char* filename);

#endif
