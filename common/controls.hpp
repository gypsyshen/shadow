/*
@HotKeys Left_Shift: switch the shadow methods.
@HotKeys Left_Alt: switch the display mode when using Hierarchy_mipmaps and Hierarchy_depths.
@HotKeys Space: store the screenshot.
@HotKeys Number_Keys: number of texels for filtering when using Hierarchy mipmaps and Hierarchy_depths.
@HotKeys '+', '-': control the light size.
@HotKeys 'W', 'S', 'A', 'D', 'Q', 'E': move the light.
@HotKeys Direction_Keys: move the camera.

@MouseKeys Left: light direction.
@MouseKeys Right: camera direction.
*/


#ifndef CONTROLS_HPP
#define CONTROLS_HPP

#include <glm/glm.hpp>

using namespace glm;

// scene enum
enum SCENE {
	PLANAR_RECEIVER_PLANAR_BLOCKER = 0,
	NONPLANA_RRECEIVE_RPLANAR_BLOCKER,
	PLANAR_RECEIVER_BOX_BLOCKER,
	NONPLANAR_RECEIVER_BOX_BLOCKER,
	PLANAR_RECEIVER_BUNNY_BLOCKER,
	NONPLANAR_RECEIVER_BUNNY_BLOCKER,
	PLANAR_RECEIVER_PLANAR_CLOSER_BLOCKER
};

//
int Animate();
void CheckAutoIncreaseFiltersize();

// 
int getScreenWidth();
int getScreenHeight();
int getShadowmapSize();


int getShadowmapSizeHighRes();
int getShadowmapSizeLowRes();

// save to png file with file name
void saveScreenToImage(const char *filename);
// save to png file
void saveScreenToImage();
// save buffer to png file
void saveBufferToImage(const char *filename, unsigned char * buffer, int w, int h);


// whether to do ray tracing.
int doRayTracing();

// get shadow type
int getShadowType();
void setNumShadowMethods(int n);

// light fov
float getLightFoV();

// light size
float getLightSize();

// display mode: 0 - regular, 1 - low, 2 - high
int getDisplayMode();

// number of samples
void updateNumberOfSamples();
int numberOfSamples();
void updateLayerToShow();
int layerToShow();

// spot light
void setLight(int sceneID);
void computeSpotLightMatricesFromInputes();
mat4 getLightProjectionMatrix();
mat4 getLightViewMatrix();

vec3 getLightRight();
vec3 getLightPosition();
vec3 getLightCenter();
vec3 getLightUp();
vec3 getLightInvDirection();
float getLightFoV();

// camera
void cameraConfig(vec3 &pos, vec3 &dir, vec3 &up, vec3 &right, float &fov, float &near, float &far);

void computeCameraMatricesFromInputs();
mat4 getCameraViewMatrix();
mat4 getCameraProjectionMatrix();

void setCamera(int sceneID);

// filter type of hierarchy method
int getHierarchyFilterType();

#endif