#ifndef PPM_H
#define PPM_H

namespace ppm{
	void save(const char* filename, unsigned char* data, int w, int h);
}

#endif