#include "ppm.hpp"
#include <string>

void ppm::save(const char* filename, unsigned char* data, int w, int h)
{
	FILE* pFile;
	char header[20];

	pFile = fopen(filename, "wb");

	// write "P6"
	fwrite("P6\n", 3, 1, pFile);

	// write "width height"
	sprintf(header, "%d %d\n", w, h);
	fwrite(header, strlen(header), 1, pFile);

	// writeh "255"
	fwrite("255\n", 4, 1, pFile);

	// write rgb data
	fwrite(data, w*h * 3, 1, pFile);

	fclose(pFile);
}