// Include GLFW
#include <glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in tutorialXXX.cpp. This is a hack to keep the tutorials simple. Please avoid this.

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "controls.hpp"


//#include <fstream>
#include <vector>
#include <iostream>
#include <ctime>
#include <string.h>

#include "lodepng.hpp"
#include "ppm.hpp"

using namespace std;


////////////////////////////////////////////////////////////
/// variables
////////////////////////////////////////////////////////////
int numRound = 1; // number of round of automatically increasing number of samples

int animate = 0; // 0: pause animation. 1: do animation

int numSamples = 1; // default number of samples

// screen width and height
int screenWidth = 800;
int screenHeight = 600;
//int screenWidth = 200;
//int screenHeight = 150;

// filter type of the mipmap method
int hierarchyFilterType = 1; // z -> 0: random sampling. x -> 1: bicubic. c -> 2: bilinear

////////////////////////////////////////////////////////////
/// automatically increase the filter size 
////////////////////////////////////////////////////////////
void CheckAutoIncreaseFiltersize() {

	if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
		numSamples = 1;
		numRound = 0;
	}
}

////////////////////////////////////////////////////////////
/// pause / start the animation
////////////////////////////////////////////////////////////
int Animate() {
	
	if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
		animate = 1;
	}
	else if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
		animate = 0;
	}

	return animate;
}

////////////////////////////////////////////////////////////
/// screen width and height
////////////////////////////////////////////////////////////

int getScreenWidth() { return screenWidth; }
int getScreenHeight() { return screenHeight; }

int getShadowmapSize() { return 1024; }

int getShadowmapSizeHighRes() { return 1024; }
//int getShadowmapSizeLowRes() { return 64; }
int getShadowmapSizeLowRes() { return 1; }


////////////////////////////////////////////////////////////
/// save screen to png
////////////////////////////////////////////////////////////

void saveBufferToImage(const char *filename, unsigned char * buffer, int w, int h) {

	// 2. generate image with 4 channels
	vector<unsigned char> image;
	image.resize(w * h * 4);
	for (unsigned y = 0; y < h; y++)
		for (unsigned x = 0; x < w; x++) {
			image[4 * w * y + 4 * x + 0] = buffer[3 * w * y + 3 * x + 0];
			image[4 * w * y + 4 * x + 1] = buffer[3 * w * y + 3 * x + 1];
			image[4 * w * y + 4 * x + 2] = buffer[3 * w * y + 3 * x + 2];
			image[4 * w * y + 4 * x + 3] = 255;
		}


	// 4. store to PNG file
	vector<unsigned char> png;
	lodepng::State state; //optionally customize this one

	unsigned error = lodepng::encode(png, image, w, h, state);
	if (!error) lodepng::save_file(png, filename);

	//if there's an error, display it
	if (error) cout << "encoder error " << error << ": " << lodepng_error_text(error) << endl;
}

void saveScreenToImage_png(const char *filename) {

	int w = screenWidth;
	int h = screenHeight;

	// 1. read screen pixels
	GLint viewPort[4];
	glGetIntegerv(GL_VIEWPORT, viewPort);
	glReadBuffer(GL_FRONT);

	int w_mul_3 = w * 3;
	int buffersize = h * w_mul_3;
	unsigned char * buffer = new unsigned char[buffersize];
	glReadPixels(viewPort[0], viewPort[1], viewPort[2], viewPort[3], GL_RGB, GL_UNSIGNED_BYTE, buffer);

	for (int j = 0; j < h; j++) {
		int a = j;
		int b = h - 1 - j;
		if (a >= b) break;
		int base_a = a * w_mul_3;
		int base_b = b * w_mul_3;
		for (int i = 0; i < w_mul_3; i++) {
			int i_a = base_a + i;
			int i_b = base_b + i;
			unsigned char t = buffer[i_b];
			buffer[i_b] = buffer[i_a];
			buffer[i_a] = t;
		}
	}

	cout << "screen pixels read!" << endl;

	// 2. generate image with 4 channels
	vector<unsigned char> image;
	image.resize(w * h * 4);
	for (unsigned y = 0; y < h; y++)
		for (unsigned x = 0; x < w; x++) {
		image[4 * w * y + 4 * x + 0] = buffer[3 * w * y + 3 * x + 0];
		image[4 * w * y + 4 * x + 1] = buffer[3 * w * y + 3 * x + 1];
		image[4 * w * y + 4 * x + 2] = buffer[3 * w * y + 3 * x + 2];
		image[4 * w * y + 4 * x + 3] = 255;
		}

	// 3. file name: filename

	// 4. store to PNG file
	vector<unsigned char> png;
	lodepng::State state; //optionally customize this one

	unsigned error = lodepng::encode(png, image, w, h, state);
	if (!error) lodepng::save_file(png, filename);

	//if there's an error, display it
	if (error) cout << "encoder error " << error << ": " << lodepng_error_text(error) << endl;
}

void saveScreenToImage_ppm(const char *filename) {

	int w = screenWidth;
	int h = screenHeight;

	// 1. read screen pixels
	GLint viewPort[4];
	glGetIntegerv(GL_VIEWPORT, viewPort);
	glReadBuffer(GL_FRONT);

	int w_mul_3 = w * 3;
	int buffersize = h * w_mul_3;
	unsigned char * buffer = new unsigned char[buffersize];
	glReadPixels(viewPort[0], viewPort[1], viewPort[2], viewPort[3], GL_RGB, GL_UNSIGNED_BYTE, buffer);

	for (int j = 0; j < h; j++) {
		int a = j;
		int b = h - 1 - j;
		if (a >= b) break;
		int base_a = a * w_mul_3;
		int base_b = b * w_mul_3;
		for (int i = 0; i < w_mul_3; i++) {
			int i_a = base_a + i;
			int i_b = base_b + i;
			unsigned char t = buffer[i_b];
			buffer[i_b] = buffer[i_a];
			buffer[i_a] = t;
		}
	}

	cout << "screen pixels read!" << endl;

	// 2. generate image with 4 channels
	ppm::save(filename, buffer, w, h);
}



void saveScreenToImage() {

	if (numRound == 0) {
		char filename[50];
		int n = numberOfSamples();
		int filetype = 0; // 0: png. 1: ppm
		// name of png file
		if (filetype == 0) {
			if (n / 10 == 0) sprintf(filename, "../../screenshot/n_00%d.png", n);
			else sprintf(filename, "../../screenshot/n_0%d.png", n);
			saveScreenToImage_png(filename);
		}
		// name of ppm file
		else {
			sprintf(filename, "../../screenshot/n-%d.ppm", n);
			saveScreenToImage_ppm(filename);
		}
	}
	else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {

		// 3. file name: current time
		// current date/time based on current system
		const time_t now = time(0);
		// convert now to string form
		tm *ltm = localtime(&now);

		char filename[50];
		float year = ltm->tm_year;
		float month = ltm->tm_mon;
		float day = ltm->tm_mday;
		float hour = ltm->tm_hour;
		float minute = ltm->tm_min;
		float second = ltm->tm_sec;
		sprintf(filename, "../../screenshot/%4.f%2.f%2.f-%2.f%2.f%2.f.png", year, month, day, hour, minute, second);

		saveScreenToImage_png(filename);
	}
}


////////////////////////////////////////////////////////////
/// switch between hierarchy filter type
////////////////////////////////////////////////////////////
int doRayTracing() {
	if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS) return 1;
	return 0;
}

////////////////////////////////////////////////////////////
/// switch between hierarchy filter type
////////////////////////////////////////////////////////////
int getHierarchyFilterType() {
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS) hierarchyFilterType = 0;
	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS) hierarchyFilterType = 1;
	if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS && !(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)) hierarchyFilterType = 2;

	// default: box filtering
	return hierarchyFilterType;
}


////////////////////////////////////////////////////////////
/// update the light size
////////////////////////////////////////////////////////////

const float minLightSize = 0.0f;
//const float maxLightSize = 0.5f;
const float maxLightSize = 2.f;
const float stepLightSize = (maxLightSize - minLightSize) / 100.f;

float lightSize = minLightSize+(minLightSize + maxLightSize)*0.3;

float getLightSize() {
	if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
		if (lightSize > minLightSize) lightSize -= stepLightSize;
		if (lightSize < minLightSize) lightSize = minLightSize;
	}
	if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
		if (lightSize < maxLightSize) lightSize += stepLightSize;
		if (lightSize > maxLightSize) lightSize = maxLightSize;
	}

	return lightSize;
}

////////////////////////////////////////////////////////////
/// update shadow type
////////////////////////////////////////////////////////////
int shadowType = 2;

// 0: pcss, 1: vssm, 2: new method

int numShadowMethods = 1;
int getShadowType() {
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
		shadowType = 0;
	}
	else if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS) {
		shadowType = 1;
	}
	else if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
		shadowType = 2;
	}
	return shadowType;
}

void setNumShadowMethods(int n) {
	numShadowMethods = n;
	cout << numShadowMethods << " shadow techniques in the system." << endl;
}

////////////////////////////////////////////////////////////
/// update the mode of display
////////////////////////////////////////////////////////////

// 0: shadow, 1: weight, 2: shadow in a layer, 3: shadow with weight, 4: total weight, 5: estimated filter size
int displayMode = 0;
int getDisplayMode() {
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS && !(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)) {
		displayMode = 1;
	}
	else if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
		displayMode = 2;
	}
	else if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		displayMode = 0;
	}
	else if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
		displayMode = 3;
	}
	else if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) {
		displayMode = 4;
	}
	else if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) {
		displayMode = 5;
	}
	return displayMode;
}

////////////////////////////////////////////////////////////
/// update the number of samples, or layers
////////////////////////////////////////////////////////////

int numOrLayer = 0; // 0: number, 1: layer
void checkNumOrLayer() {
	if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
		numOrLayer = 0;
		cout << "Choose Sample Number:" << endl;
	}
	else if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS) {
		numOrLayer = 1;
		cout << "Choose Layer:" << endl;
	}
}

int layer = 0;
int layerToShow() { return layer; }

void updateLayerToShow() {

	checkNumOrLayer();

	if (numOrLayer == 1) {
		if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS) {
			layer = 0;
		}
		else if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
			layer = 1;
		}
		else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
			layer = 2;
		}
		else if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
			layer = 3;
		}
		else if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS) {
			layer = 4;
		}
		else if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS) {
			layer = 5;
		}
		else if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS) {
			layer = 6;
		}
		else if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS) {
			layer = 7;
		}
		else if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS) {
			layer = 8;
		}
		else if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS) {
			layer = 9;
		}
	}
}

int numberOfSamples() { return numSamples; }

// automatically increase the number of samples
void updateNumberOfSamplesAutoIncrease() {
	
//	numSamples++;

	static float numSamplesF = 1.f;
	numSamplesF += 0.5f;

	numSamples = (int)numSamplesF;

	if (numSamples > 23) {
		numSamples = 1;
		numRound = 1;
	}
}

void updateNumberOfSamples() {

	if (numRound == 0) {
		updateNumberOfSamplesAutoIncrease();
		return;
	}
	
	checkNumOrLayer();

	if (numOrLayer == 0) {

		if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
			numSamples = 1;
		}
		else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
			numSamples = 2;
		}
		else if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
			numSamples = 3;
		}
		else if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS) {
			numSamples = 4;
		}
		else if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS) {
			numSamples = 5;
		}
		else if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS) {
			numSamples = 6;
		}
		else if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS) {
			numSamples = 7;
		}
		else if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS) {
			numSamples = 8;
		}
		else if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS) {
			numSamples = 9;
		}
	}	
}


////////////////////////////////////////////////////////////
/// compute spot light matrices from inputs
////////////////////////////////////////////////////////////

vec3 lightPos = vec3(3.898989, 11.36871, 1.12715);
vec3 lightDir;
vec3 lightUp;
vec3 lightRight;

float lightHorizontalAngle = 3.324;
float lightVerticalAngle = -1.494;
// float lightInitialFoV = 90.0f;
float lightInitialFoV = 45.0f;

void setLight(int sceneID) {

	switch (sceneID) {
	case SCENE::NONPLANA_RRECEIVE_RPLANAR_BLOCKER:
		lightPos = vec3(3.898989, 11.36871, 1.12715);
		lightHorizontalAngle = 3.324;
		lightVerticalAngle = -1.494;
		break;
	case SCENE::PLANAR_RECEIVER_PLANAR_BLOCKER:
		lightPos = vec3(2.22829, 12.527941, 2.316481);
		lightHorizontalAngle = 3.337;
		lightVerticalAngle = -1.454;
		break;
	case SCENE::PLANAR_RECEIVER_BOX_BLOCKER:
		lightPos = vec3(11.942529, 14.309753, 9.630451);
		lightHorizontalAngle = 4.012;
		lightVerticalAngle = -0.692;
		break;
	case SCENE::NONPLANAR_RECEIVER_BOX_BLOCKER:
		lightPos = vec3(8.16935, 12.597523, 7.450337);
		lightHorizontalAngle = 3.834007;
		lightVerticalAngle = -1.037;
		break;
	case SCENE::NONPLANAR_RECEIVER_BUNNY_BLOCKER:
		lightPos = vec3(1.085126, 6.776277, -4.100239);
		lightHorizontalAngle = 5.83901;
		lightVerticalAngle = -1.112001;
		break;
	case SCENE::PLANAR_RECEIVER_BUNNY_BLOCKER:
		lightPos = vec3(1.338855, 5.238005, 4.162691);
		lightHorizontalAngle = 9.589996;
		lightVerticalAngle = -0.780999;
		break;
	case SCENE::PLANAR_RECEIVER_PLANAR_CLOSER_BLOCKER:
		lightPos = vec3(1.671257, 11.36871, 1.539907);
		lightHorizontalAngle = 3.324;
		lightVerticalAngle = -1.494;
		break;
	default:
		lightPos = vec3(3.898989, 11.36871, 1.12715);
		lightHorizontalAngle = 3.324;
		lightVerticalAngle = -1.494;
	}	
}

vec3 getLightRight() {
	return lightRight;
}
vec3 getLightPosition() {
	return lightPos;
}
vec3 getLightUp() {
	return lightUp;
}
vec3 getLightInvDirection() {
	return -lightDir;
}
float getLightFoV() {
	return lightInitialFoV;
}

mat4 LightViewMatrix;
mat4 LightProjectionMatrix;

mat4 getLightProjectionMatrix() {
	return LightProjectionMatrix;
}
mat4 getLightViewMatrix() {
	return LightViewMatrix;
}

float lightMoveSpeed = 2.0f;
float lightRotateSpeed = 0.5f;

void updateLightDirections() {

	lightDir = glm::vec3(
		cos(lightVerticalAngle) * sin(lightHorizontalAngle),
		sin(lightVerticalAngle),
		cos(lightVerticalAngle) * cos(lightHorizontalAngle)
		);
	lightRight = glm::vec3(
		sin(lightHorizontalAngle - 3.14f / 2.0f),
		0,
		cos(lightHorizontalAngle - 3.14f / 2.0f)
		);
	lightUp = glm::cross(glm::normalize(lightRight), glm::normalize(lightDir));
}

void computeSpotLightMatricesFromInputes() {

	// -----------------------------------
	// print light position, horizontal & verticle angle
	// -----------------------------------
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
		printf("lightHorizontalAngle: %f\n", lightHorizontalAngle);
		printf("lightVerticalAngle: %f\n", lightVerticalAngle);
		printf("lightPos: %f, %f, %f\n\n\n", lightPos.x, lightPos.y, lightPos.z);
	}

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// ---------------------------------
	// change angle
	// ---------------------------------
	// Get mouse position
	static double lastXPos = screenWidth / 2, lastYPos = screenHeight / 2;
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {

		lightHorizontalAngle += 0.001f * float(lastXPos - xpos);
		lightVerticalAngle += 0.001f * float(lastYPos - ypos);

		updateLightDirections();
	}

	static bool initialized = false;
	if (!initialized) {
		updateLightDirections();
		initialized = true;
	}


	// ---------------------------------
	// change position
	// ---------------------------------

	// w: move forward
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		lightPos += lightDir * deltaTime * lightMoveSpeed;
	}
	// s: move backward
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		lightPos -= lightDir * deltaTime * lightMoveSpeed;
	}
	// d: strafe right
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
		lightPos += getLightRight() * deltaTime * lightMoveSpeed;
	}
	// a: strafe left
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		lightPos -= getLightRight() * deltaTime * lightMoveSpeed;
	}
	// q: move up
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS){
		lightPos += lightUp * deltaTime * lightMoveSpeed;
	}
	// e: move down
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS){
		lightPos -= lightUp * deltaTime * lightMoveSpeed;
	}


	// ---------------------------------
	// update matrices
	// ---------------------------------

	LightProjectionMatrix = glm::perspective<float>(lightInitialFoV, 1.0f, 2.f, 200.0f);
	LightViewMatrix = glm::lookAt(lightPos, lightPos + lightDir, lightUp);

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
	lastXPos = xpos;
	lastYPos = ypos;
}

////////////////////////////////////////////////////////////
/// compute camera matrices from inputs
////////////////////////////////////////////////////////////

mat4 cameraViewMatrix;
mat4 cameraProjectionMatrix;

mat4 getCameraViewMatrix(){
	return cameraViewMatrix;
}
mat4 getCameraProjectionMatrix(){
	return cameraProjectionMatrix;
}

// Initial position : on +Z
vec3 cameraPos = glm::vec3(-3.681711, 5.624032, 3.589179);
// Initial horizontal angle : toward -Z
float cameraHorizontalAngle = -4.329003;
// Initial vertical angle : none
float cameraVerticalAngle = -0.871;
// Initial Field of View
float cameraFoV = 45.0f;
float cameraNearPlane = 0.05f;
float cameraFarPlane = 100.f;

// float cameraSpeed = 15.f; // 3 units / second
float cameraSpeed = 5.f; // 3 units / second

void setCamera(int sceneID) {

	switch (sceneID) {
	case SCENE::NONPLANA_RRECEIVE_RPLANAR_BLOCKER:
		cameraPos = glm::vec3(-3.681711, 5.624032, 3.589179);
		cameraHorizontalAngle = -4.329003;
		cameraVerticalAngle = -0.871;
		break;
	case SCENE::PLANAR_RECEIVER_PLANAR_BLOCKER:
		cameraPos = glm::vec3(-4.694803, 9.647224, 4.780253);
		cameraHorizontalAngle = -4.174005;
		cameraVerticalAngle = -0.852;
		break;
	case SCENE::PLANAR_RECEIVER_BOX_BLOCKER:
		cameraPos = glm::vec3(-4.479934, 12.62438, 11.385552);
		cameraHorizontalAngle = -3.209011;
		cameraVerticalAngle = -0.597;
		break;
	case SCENE::NONPLANAR_RECEIVER_BOX_BLOCKER:
		cameraPos = glm::vec3(-5.750581, 10.966814, 8.186841);
		cameraHorizontalAngle = -3.753007;
		cameraVerticalAngle = -0.813;
		break;
	case SCENE::NONPLANAR_RECEIVER_BUNNY_BLOCKER:
		cameraPos = glm::vec3(0.268382, 3.621564, 1.59372);
		cameraHorizontalAngle = -5.092008;
		cameraVerticalAngle = -1.188001;
		break;
	case SCENE::PLANAR_RECEIVER_BUNNY_BLOCKER:
		cameraPos = glm::vec3(6.425089, 4.378969, -4.787257);
		cameraHorizontalAngle = -8.901007;
		cameraVerticalAngle = -0.652;
		break;
	case SCENE::PLANAR_RECEIVER_PLANAR_CLOSER_BLOCKER:
		cameraPos = glm::vec3(-3.690665, 3.725205, 3.910748);
		cameraHorizontalAngle = -4.227004;
		cameraVerticalAngle = -0.485999;
		break;
	default:
		cameraPos = glm::vec3(-4.694803, 9.647224, 4.780253);
		cameraHorizontalAngle = -4.174005;
		cameraVerticalAngle = -0.852;
	}	
}

void cameraConfig(vec3 &pos, vec3 &dir, vec3 &up, vec3 &right, float &fov, float &near, float &far) {
	
	pos = cameraPos;
	
	// Direction : Spherical coordinates to Cartesian coordinates conversion
	dir = glm::vec3(
		cos(cameraVerticalAngle) * sin(cameraHorizontalAngle),
		sin(cameraVerticalAngle),
		cos(cameraVerticalAngle) * cos(cameraHorizontalAngle)
		);
	dir = normalize(dir);

	// Right vector
	right = glm::vec3(
		sin(cameraHorizontalAngle - 3.14f / 2.0f),
		0,
		cos(cameraHorizontalAngle - 3.14f / 2.0f)
		);
	right = normalize(right);

	// Up vector
	up = glm::cross(right, dir);
	up = normalize(up);

	// fov
	fov = cameraFoV;

	// near and far planes
	near = cameraNearPlane;
	far = cameraFarPlane;
}

void computeCameraMatricesFromInputs() {

	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
		printf("cameraHorizontalAngle: %f\n", cameraHorizontalAngle);
		printf("cameraVerticalAngle: %f\n", cameraVerticalAngle);
		printf("cameraPos: %f, %f, %f\n\n\n", cameraPos.x, cameraPos.y, cameraPos.z);
	}

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	static double lastXPos = screenWidth / 2, lastYPos = screenHeight / 2;
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	//	glfwSetCursorPos(window, 1024 / 2, 768 / 2);
	glfwSetCursorPos(window, xpos, ypos);

	static glm::vec3 direction;
	static glm::vec3 right;
	static glm::vec3 up;

	static bool initialized = false;
	if (!initialized) {
		// Direction : Spherical coordinates to Cartesian coordinates conversion
		direction = glm::vec3(
			cos(cameraVerticalAngle) * sin(cameraHorizontalAngle),
			sin(cameraVerticalAngle),
			cos(cameraVerticalAngle) * cos(cameraHorizontalAngle)
			);
		direction = normalize(direction);

		// Right vector
		right = glm::vec3(
			sin(cameraHorizontalAngle - 3.14f / 2.0f),
			0,
			cos(cameraHorizontalAngle - 3.14f / 2.0f)
			);
		right = normalize(right);

		// Up vector
		up = glm::cross(right, direction);
		up = normalize(up);

		initialized = true;
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
		// Compute new orientation
		//	horizontalAngle += mouseSpeed * float(1024 / 2 - xpos);
		//	verticalAngle += mouseSpeed * float(768 / 2 - ypos);
		cameraHorizontalAngle += 0.001f * float(lastXPos - xpos);
		cameraVerticalAngle += 0.001f * float(lastYPos - ypos);

		// Direction : Spherical coordinates to Cartesian coordinates conversion
		direction = glm::vec3(
			cos(cameraVerticalAngle) * sin(cameraHorizontalAngle),
			sin(cameraVerticalAngle),
			cos(cameraVerticalAngle) * cos(cameraHorizontalAngle)
			);
		direction = normalize(direction);

		// Right vector
		right = glm::vec3(
			sin(cameraHorizontalAngle - 3.14f / 2.0f),
			0,
			cos(cameraHorizontalAngle - 3.14f / 2.0f)
			);
		right = normalize(right);

		// Up vector
		up = glm::cross(right, direction);
		up = normalize(up);
	}

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
		cameraPos += direction * deltaTime * cameraSpeed;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
		cameraPos -= direction * deltaTime * cameraSpeed;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
		cameraPos += right * deltaTime * cameraSpeed;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
		cameraPos -= right * deltaTime * cameraSpeed;
	}

	// Update FOV
//	float FoV = cameraInitialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.

	// Projection matrix : 45?Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
//	cameraProjectionMatrix = glm::perspective(cameraFoV, 4.0f / 3.0f, 0.05f, 100.0f);
	cameraProjectionMatrix = glm::perspective(cameraFoV, 4.0f / 3.0f, cameraNearPlane, cameraFarPlane);
	// Camera matrix
	cameraViewMatrix = glm::lookAt(
		cameraPos,           // Camera is here
		cameraPos + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
		);

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
	lastXPos = xpos;
	lastYPos = ypos;
}